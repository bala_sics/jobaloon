//
//  MyCandidateTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 09/10/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MyCandidateTableViewController: UITableViewController {

    var offerObj : Offer!
    var isFirstTime : Bool = true
    var indexCount : NSInteger!
    var _activityIndicatorView : UIActivityIndicatorView?

    var arrayUsers : NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = offerObj.offerName
        self.tableView.backgroundColor = UIColor(red: 241.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        indexCount = 0;
        getMyCandidates()
        setLoadingFooter()
        _activityIndicatorView?.stopAnimating()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- GET MY CANDIDATES
    func getMyCandidates (){
        if(isFirstTime){
            SVProgressHUD.show()
        }
        let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken , "offerId" : offerObj.offerId , "index" : indexCount , "userid" : Person.currentPerson().userId , "compony_id" : Person.currentPerson().userId]
        MyCandidate.getMyCandidates(data: params) { (success, result, error) -> Void in
            if(self.indexCount == 0) { self.arrayUsers.removeAllObjects() }
            if (success){
                SVProgressHUD.dismiss()
                self.arrayUsers.removeAllObjects()
                self.arrayUsers.addObjectsFromArray(result as! [AnyObject])
            }else{
                self.isFirstTime ? SVProgressHUD.showErrorWithStatus(error?.localizedDescription) : SVProgressHUD.dismiss()
            }
            self.isFirstTime = false
            self._activityIndicatorView?.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    //MARK:- TABLEVIEW DELEGATES
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayUsers.count
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell  = tableView.dequeueReusableCellWithIdentifier("cel")
        if (cell == nil){
            cell = MyCandidateTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        (cell as! MyCandidateTableViewCell).configureCell(arrayUsers[indexPath.row] as! MyCandidate)
        (cell as! MyCandidateTableViewCell).buttonSeeProfile.tag = indexPath.row
        (cell as! MyCandidateTableViewCell).buttonSeeProfile.addTarget(self, action: "onButtonSeeProfilePressed:", forControlEvents: UIControlEvents.TouchUpInside)
        (cell as! MyCandidateTableViewCell).buttonChat.tag = indexPath.row
        (cell as! MyCandidateTableViewCell).buttonChat.addTarget(self, action: "onButtonChatPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        return cell!
    }
    //MARK:- SETTING FOOTER LOADING VIEW
    func setLoadingFooter (){
        let frame : CGRect =  CGRectMake(0, 0, self.view.frame.size.width, 30)
        let aframe : CGRect = CGRectMake(self.view.center.x - 10, 20, 5, 5)
        let loadingView : UIView = UIView(frame: frame)
        _activityIndicatorView = UIActivityIndicatorView(frame: aframe)
        _activityIndicatorView?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        _activityIndicatorView?.startAnimating()
        loadingView.addSubview(_activityIndicatorView!)
        self.tableView.tableFooterView = loadingView
    }
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if (self.tableView.contentOffset.y<0)/* scroll from top */
        {
            
        }else if (self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height) && arrayUsers.count > 0 ){
            indexCount = indexCount + 1
            self.setLoadingFooter()
            self.getMyCandidates()
        }
    }

    //MARK:- BUTTON ACTION
    //MARK:- SEE PROFILE BUTTON ACTION
    func onButtonSeeProfilePressed (sender : UIButton){
        let cand : MyCandidate = arrayUsers[sender.tag] as! MyCandidate
          let viewprofile : ViewProfileViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ViewProfileVC") as! ViewProfileViewController
        viewprofile.candidateObj = cand
       self.navigationController?.pushViewController(viewprofile, animated: true)
    }
    
    //MARK:- CHAT BUTTON ACTION
    func onButtonChatPressed (sender : UIButton){
         let cand : MyCandidate = arrayUsers[sender.tag] as! MyCandidate
        let chat : ChatViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ChatVC") as! ChatViewController
        chat.toUserId = cand.candidateUserId!
        chat.toUserName = cand.candidateUsername
        self.navigationController?.pushViewController(chat, animated: true)

    }
}
