//
//  LoginTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 7/8/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class LoginTableViewController: UITableViewController,UITextFieldDelegate,BMAlertDelegate {
    
    @IBOutlet var textFieldEmail : UITextField!
    @IBOutlet var textFieldPassword : UITextField!
    @IBOutlet var buttonLogin : UIButton!
    var textFieldActive : UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //CANCEL NAVIGATION BAR BUTTON
        let cancelButton : UIBarButtonItem = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.Plain, target: self, action: "CancelButtonPressed")
        self.navigationItem.rightBarButtonItem = cancelButton
        
        //TABLEVIEW BACKGROUND COLOR
        self.tableView.backgroundColor = UIColor(red: 241.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        //TEXTFIELD PADDING
        textFieldEmail.setLeftPadding(20)
        textFieldEmail.layer.cornerRadius = 3.0
        textFieldPassword.setLeftPadding(20)
        textFieldPassword.layer.cornerRadius = 3.0
        buttonLogin.layer.cornerRadius = 3.0
        
        if(IS_IPHONE4){ self.tableView.tableHeaderView = nil }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- ACTIVATE EMAIL FIELD
    func activateEmailField(){
        textFieldEmail.becomeFirstResponder()
        
    }
    
    //MARK:- TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(textField: UITextField) {
        textFieldActive = textField
    }
    
    //MARK:- BUTTON ACTIONS
    //MARK:- CANCEL BUTTON
    func CancelButtonPressed (){
        textFieldActive.resignFirstResponder()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:- LOGIN BUTTON ACTION
    @IBAction func loginButtonPressed (sender : UIButton){
        textFieldActive.resignFirstResponder()
        if(textFieldEmail.text == "" || !textFieldEmail.IsValidEmail()){
            showAlert("Introduzca correo electrónico válida")
        }else if(textFieldPassword.text == ""){
            showAlert("Por favor, ingrese contraseña")
        }else{
            loginWithJson()
        }
    }
    
    //MARK:- SHOW ALERT
    func showAlert (text : String){
        let alertView : BMAlert = (NSBundle.mainBundle().loadNibNamed("BMAlert", owner: self, options: nil) as NSArray)[0] as! BMAlert
        alertView.delegate = self
        alertView.configureView(title: "Error", message: text, buttonTitle: "Ok", labelAlignment: NSTextAlignment.Center)
        KGModal.sharedInstance().showWithContentView(alertView)
    }
    
    func buttonInAlertPressed(sender: UIButton) {
        KGModal.sharedInstance().hideAnimated(true)
    }
    
    //MARK:- LOGIN WITH JSON
    func loginWithJson (){
        SVProgressHUD.show()
        Person.login(textFieldEmail.text!, password: textFieldPassword.text!) { (success, result, error) -> Void in
            if(success){
                SVProgressHUD.dismiss()
                AppDelegate.getAppdelegateInstance().didLogin()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
}
