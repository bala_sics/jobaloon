//
//  AccountTypeViewController.swift
//  Jobaloon
//
//  Created by Bala on 7/7/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class AccountTypeViewController: UIViewController {
    @IBOutlet var constraintBottom : NSLayoutConstraint!
    @IBOutlet var imageViewBG : UIImageView!
    @IBOutlet var constraintCentre : NSLayoutConstraint!
    @IBOutlet var constraintLabel : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        constraintBottom.constant = IS_IPHONE4 ? 5.0 : 63.0
        constraintCentre.constant = IS_IPHONE4 ? 15.0 : 32.0
        constraintLabel.constant = IS_IPHONE6 ? 100 : IS_IPHONE6PLUS  ? 120 : 80

        imageViewBG.image = UIImage(named: IS_IPHONE4 ? "AccountSelectionBG4" : "AccountSelectionBG")
        
        let swipe : UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "goBack")
        swipe.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipe)

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true

    }
    //MARK:- SWIPE GESTURE ACTION
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- BUTTON ACTIONS
    //MARK:- LOOKING FOR JOBS
    @IBAction func lookingForJobsButtonPressed (sender : UIButton){
        USERDICT.setValue("Jobseeker", forKey: "type")
        let jobType : JobTypeTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("JobTypeVC") as! JobTypeTableViewController
        jobType.fromSideMenu = "NO"
        self.navigationController?.pushViewController(jobType, animated: true)
    }
    
    //MARK:- LOOKING FOR JOBSEEKERS
    @IBAction func lookingForJobseekersButtonPressed (sender : UIButton){
        USERDICT.setValue("Company", forKey: "type")
        let jobType : JobTypeTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("JobTypeVC") as! JobTypeTableViewController
        jobType.fromSideMenu = "NO"
        self.navigationController?.pushViewController(jobType, animated: true)
        
    }
    
}
