//
//  MyApplicationsTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 9/25/15.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MyApplicationsTableViewController: UITableViewController {

    var arrayApplications : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Inscripciones"
        getMyApplications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- GET MY APPLICATIONS FROM JSON
    func getMyApplications (){
        SVProgressHUD.show()
        MyApplication.getMyApplications { (success, result, error) -> Void in
            if(success){
                self.arrayApplications.removeAllObjects()
                self.arrayApplications.addObjectsFromArray(result as! [AnyObject])
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    // MARK: - TABLE VIEW DELEGATES
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayApplications.count
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        // iOS 7
        if(self.tableView.respondsToSelector(Selector("setSeparatorInset:"))){
            self.tableView.separatorInset = UIEdgeInsetsZero
        }
        // iOS 8
        if(self.tableView.respondsToSelector(Selector("setLayoutMargins:"))){
            if #available(iOS 8.0, *) {
                self.tableView.layoutMargins = UIEdgeInsetsZero
            } else {
                // Fallback on earlier versions
            };
        }
        if(cell.respondsToSelector(Selector("setLayoutMargins:"))){
            if #available(iOS 8.0, *) {
                cell.layoutMargins = UIEdgeInsetsZero
            } else {
                // Fallback on earlier versions
            };
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 100
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell  = tableView.dequeueReusableCellWithIdentifier("cel")
        if (cell == nil){
            cell = MyApplicationsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            
        }
        (cell as! MyApplicationsTableViewCell).configureCell(arrayApplications[indexPath.row] as! MyApplication)
        return cell!
    }

   

}
