//
//  RegistrationTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 7/9/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation

class RegistrationTableViewController: UITableViewController,UITextFieldDelegate,BMAlertDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet var arrayTextFields : [UITextField]!
    @IBOutlet var ImageViewThumbnail : UIImageView!
    @IBOutlet var viewHeader : UIView!
    @IBOutlet var buttonLogout : UIButton!
    @IBOutlet var ImageViewPlay : UIImageView!
    @IBOutlet var activityIndicatorView : UIActivityIndicatorView!

    
    
    var selectedVideoData : NSData!
    var movieUrl : NSURL!
    var moviePlayer : MPMoviePlayerController!
    var alertView : BMAlert!
    var imagePicker : UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Mi perfil"
        
        //TABLEVIEW BACKGROUND COLOR
        self.tableView.backgroundColor = UIColor(red: 241.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        viewHeader.frame = CGRectMake(0, 0, viewHeader.frame.width, IS_IPHONE6 ? 440 : IS_IPHONE6PLUS ? 470 : 387)
        for textFieldObj in arrayTextFields{
            textFieldObj.setLeftPadding(20)
            textFieldObj.layer.cornerRadius = 3.0
        }
        
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onThumbnailImageTapped")
        tapGesture.numberOfTapsRequired = 1
        ImageViewThumbnail.addGestureRecognizer(tapGesture)
        
//        ImageViewPlay = UIImageView(frame: CGRectMake(ImageViewThumbnail.frame.origin.x + (ImageViewThumbnail.frame.width/2 - 50),ImageViewThumbnail.frame.origin.y + (ImageViewThumbnail.frame.height/2 - 50), 100, 100))
//        ImageViewPlay.image = UIImage(named: "Play")
//        ImageViewThumbnail.superview!.addSubview(ImageViewPlay)
        ImageViewPlay.hidden = true
        buttonLogout.setTitle("¿¿Ya eres miembro?? registrarse", forState: UIControlState.Normal)
        if (Person.currentPerson().userId != nil){
            loadData()
            buttonLogout.setTitle("Cerrar sesión", forState: UIControlState.Normal)

        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        if(moviePlayer != nil){
            moviePlayer.stop()
            moviePlayer = nil
        }
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- TEXTFIELD DELEGATES
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- LOAD DATA
    func loadData (){
        arrayTextFields[0].text = Person.currentPerson().username
        arrayTextFields[1].text = Person.currentPerson().userEmail
        arrayTextFields[2].text = Person.currentPerson().userPassword
        arrayTextFields[3].text = Person.currentPerson().userExperience1
        arrayTextFields[4].text = Person.currentPerson().userExperience2
        arrayTextFields[5].text = Person.currentPerson().userExperience3
        ImageViewThumbnail.sd_setImageWithURL(NSURL(string: imagePath + Person.currentPerson().userImage), placeholderImage: UIImage(named: "VideoPlaceholder"))
        movieUrl = NSURL(string: videoPath + Person.currentPerson().userVideo)
        ImageViewPlay.hidden = false
    }
    
    //MARK:- BUTTON ACTION
    //MARK:- RECORD VIDEO BUTTON ACTION
    @IBAction func recordVideoButtonPressed (){
        if(moviePlayer != nil) {
            moviePlayer.stop()
            moviePlayer.view.removeFromSuperview()
            moviePlayer = nil
        }
        imagePicker = imagePicker == nil ? UIImagePickerController() : imagePicker
        imagePicker.view.backgroundColor = UIColor.whiteColor()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes =  [kUTTypeMovie as String]
        imagePicker.videoMaximumDuration = 15
        imagePicker.videoQuality = UIImagePickerControllerQualityType.TypeMedium;
        let hasCamera : Bool = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        imagePicker.sourceType = hasCamera ? UIImagePickerControllerSourceType.Camera : UIImagePickerControllerSourceType.PhotoLibrary
        imagePicker.cameraDevice = UIImagePickerControllerCameraDevice.Front
        // self.presentViewController(imagePicker, animated: true, completion: nil)
        self.presentViewController(imagePicker, animated: true) { () -> Void in
           // self.showAlert(title: "Cómo grabar tu vídeo", message: "Tienes 10 segundos para explicar quién eres. Aquí tienes un ejemplo de discurso que hemos visto que es efectivo \n \n Ho;a me ilamo..., tengo... anos y estoy buscando trabajo de... \n Teneis que contratarme porque soy una persona muy... y... \n muchas gracias", buttonTitle: "Ok")
             self.showAlert(title: "Cómo grabar tu vídeo", message: "Tienes 10 segundos para explicar quién eres. Aquí tienes un ejemplo de pitch que sabemos que funciona \n Hola, me llamo (nombre), tengo (edad) y debéis contratarme porque soy una persona muy (cualidad) y (cualidad).\n Muchas gracias.", buttonTitle: "Ok")
        }
    }
    
    //MARK:- IMAGEPICKER CONTROLLER DELEGATE
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
        movieUrl = info[UIImagePickerControllerMediaURL] as! NSURL!
        selectedVideoData = NSData(contentsOfURL: movieUrl)!
        let asset : AVAsset = AVAsset(URL: movieUrl)
        let imageGenerator : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let time : CMTime = CMTimeMake(1, 1)
        let imageRef  = try? imageGenerator.copyCGImageAtTime(time, actualTime: nil)
        ImageViewThumbnail.image = UIImage(CGImage: imageRef!)
        ImageViewPlay.hidden = false
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:- SAVE PROFILE BUTTON ACTION
    @IBAction func saveProfileButtonPressed(sender : UIButton){
        if(Person.currentPerson().userId == nil){
            if(selectedVideoData == nil){
                showAlert(title: "¿Quieres que te seleccionen?", message: "¡Si no grabas tu vídeo las empresas no te entrevistarán!", buttonTitle: "Grabar vídeo")
            }else if (!arrayTextFields[1].IsValidEmail()){
                showAlert(title: "Error", message: " Introduzca un correo electrónico válido", buttonTitle: "Ok")
            }else if (!verifyFields()){
                showAlert(title: "Error", message: "Rellene todos los campos", buttonTitle: "Ok")
            }else{
                if(Person.currentPerson().userId == nil){
                    createProfileWithJson()
                }
            }
        }else{
            if (!arrayTextFields[1].IsValidEmail()){
                showAlert(title: "Error", message: "Introduzca correo electrónico válida", buttonTitle: "Ok")
            }else if (!verifyFields()){
                showAlert(title: "Error", message: "Rellene todos los campos", buttonTitle: "Ok")
            }else{
                    editProflieWithJson()
            }
        }
    }
    
    //MARK:- SHOW CUSTOM ALERT
    func showAlert(title title : String, message : String , buttonTitle : String){
        let alertView : BMAlert = (NSBundle.mainBundle().loadNibNamed("BMAlert", owner: self, options: nil) as NSArray)[0] as! BMAlert
        alertView.delegate = self
        alertView.configureView(title: title, message: message, buttonTitle: buttonTitle, labelAlignment: NSTextAlignment.Center)
        KGModal.sharedInstance().showWithContentView(alertView)
    }
    
    func buttonInAlertPressed(sender: UIButton) {
        KGModal.sharedInstance().hideAnimated(true)
        if(sender.titleLabel?.text == "Grabar vídeo") { recordVideoButtonPressed()  }
    }
    
    //MARK:- LOGOUT BUTTON ACTION
    @IBAction func onLogoutButtonPressed (sender : UIButton){
        if(Person.currentPerson().userId == nil){
            Person.logout()
            AppDelegate.getAppdelegateInstance().didLogout()
        }else{
            SVProgressHUD.show()
            Person.logout({ (success, error) -> Void in
                if(success){
                    SVProgressHUD.dismiss()
                    Person.logout()
                    AppDelegate.getAppdelegateInstance().didLogout()
                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            })
        }
    }
    
    //MARK:- VERIFYING TEXTFIELD IS EMPTY OR NOT
    func verifyFields() -> Bool
    {
        var success : Bool = true
        for (var i = 0 ; i < 3 ; i++) {
            let textFieldObj : UITextField = arrayTextFields[i]
            if textFieldObj.text!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0
            {
                success = false
            }
        }
        return success
    }
    
    //MARK:- MPMOVIE PLAYER
    func onThumbnailImageTapped(){
        if(movieUrl == nil){ return }
        moviePlayer = MPMoviePlayerController(contentURL: movieUrl)
        moviePlayer.scalingMode = MPMovieScalingMode.AspectFill
        moviePlayer.view.frame = ImageViewThumbnail.frame
        moviePlayer.movieSourceType = MPMovieSourceType.File
//        moviePlayer.prepareToPlay()
        moviePlayer.controlStyle = MPMovieControlStyle.None
        moviePlayer.initialPlaybackTime = -1.0;
        moviePlayer.play()
//        ImageViewThumbnail.superview!.insertSubview(moviePlayer.view, belowSubview: ImageViewPlay)
        activityIndicatorView.startAnimating()
//       // ImageViewThumbnail.superview!.addSubview(moviePlayer.view)
//        ImageViewThumbnail.superview!.insertSubview(moviePlayer.view, belowSubview: ImageViewPlay)
        ImageViewPlay.hidden = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "playBackCompleted:", name: MPMoviePlayerPlaybackDidFinishNotification, object: moviePlayer)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "movieplayerStateChanged:", name: MPMoviePlayerLoadStateDidChangeNotification, object: moviePlayer)

        moviePlayer.view.userInteractionEnabled = true
        
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onMoviePlayerTapped")
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate = self
        moviePlayer.view.addGestureRecognizer(tapGesture)
    }
    
    func onMoviePlayerTapped(){
        if(moviePlayer.playbackState.rawValue == 1){
            moviePlayer.pause()
            ImageViewPlay.hidden = false
        }else{
            moviePlayer.play()
            ImageViewPlay.hidden = true
        }
    }
    
    //MARK:- DID FINISH PLAYING VIDEO NOTIFICATION
    func playBackCompleted (aNotification : NSNotification){
        let player : MPMoviePlayerController = aNotification.object as! MPMoviePlayerController
        player.stop()
        player.view.removeFromSuperview()
        ImageViewPlay.hidden = false
        activityIndicatorView.stopAnimating()

    }
    
    func movieplayerStateChanged (aNotification : NSNotification){
        let player : MPMoviePlayerController = aNotification.object as! MPMoviePlayerController
        if( player.loadState ==  MPMovieLoadState.Playable ||  player.loadState ==  MPMovieLoadState.PlaythroughOK)
        {
            self.performSelector("insertPlayerView", withObject: nil, afterDelay: 1.5)
        }

    }
    
    
    func insertPlayerView (){
        activityIndicatorView.stopAnimating()
        ImageViewThumbnail.superview!.insertSubview(moviePlayer.view, belowSubview: ImageViewPlay)

    }
    
    //MARK:- GESTURE RECOGNISER DELEGATES
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //MARK:- PROFILE CREATION WITH JSON
    func createProfileWithJson(){
        SVProgressHUD.show()
        Person.createProfile(name: arrayTextFields[0].text!, email: arrayTextFields[1].text!, password: arrayTextFields[2].text!, address: USERDICT["address"] as! String, userType: "Jobseeker", experience1: arrayTextFields[3].text!, exp2: arrayTextFields[4].text!, exp3: arrayTextFields[5].text!, thumbnailImage: ImageViewThumbnail.image!, videoData: selectedVideoData, label: USERDICT["jobtype"] as! String) { (success, result, error) -> Void in
            if(success){
                SVProgressHUD.dismiss()
                //  self.insertJobType()
                NSNotificationCenter.defaultCenter().postNotificationName("ProfileCreated", object: nil)
                self.navigationController?.popToRootViewControllerAnimated(true)
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    func insertJobType (){
        //52.0.125.31/JobBox/json/add_jobType.php?userid=522&job_type=Camerero/a
        let params : NSDictionary = ["userid" : Person.currentPerson().userId , "job_type" : USERDICT["jobtype"] as! String]
        BMServiceManager.fetchDataFromService("add_jobType.php", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
        }
    }
    
    //MARK:- EDIT PROFILE WITH JSON
    func editProflieWithJson(){
        SVProgressHUD.show()
        let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken ,"userid" : Person.currentPerson().userId, "user_name" : arrayTextFields[0].text! , "user_email" : arrayTextFields[1].text! , "password" : arrayTextFields[2].text! , "user_address" : Person.currentPerson().userAddress ,  "experience1" : arrayTextFields[3].text! , "experience2" : arrayTextFields[4].text! , "experience3" : arrayTextFields[5].text!, "user_type" : "Jobseeker"]
        
        let imageData : NSData = UIImageJPEGRepresentation(ImageViewThumbnail.image!, 1.0)!
        var arrayImage : NSArray!
        var arrayvideo : NSArray!
        if(selectedVideoData != nil){
            arrayImage = [["value": imageData , "key" : "image"]]
            arrayvideo = [["value" :selectedVideoData , "key" : "video"]]
        }else{
            arrayImage = []
            arrayvideo = []
        }
        
        Person.editProfile(data: params, image:arrayImage , video: arrayvideo ) { (success, result, error) -> Void in
            if(success){
                SVProgressHUD.dismiss()
                self.navigationController?.popToRootViewControllerAnimated(true)
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
}
