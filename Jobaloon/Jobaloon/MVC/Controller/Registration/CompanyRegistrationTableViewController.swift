//
//  CompanyRegistrationTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 9/21/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class CompanyRegistrationTableViewController: UITableViewController {
    
    @IBOutlet var arrayTextFields : [UITextField]!
    @IBOutlet var ImageViewProfile : UIImageView!
    @IBOutlet var viewHeader : UIView!
    var imagePicker : UIImagePickerController!
    var selectedImageData : NSData?
    @IBOutlet var buttonLogout : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Mi perfil"
        
        //TABLEVIEW BACKGROUND COLOR
        self.tableView.backgroundColor = UIColor(red: 241.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        viewHeader.frame = CGRectMake(0, 0, viewHeader.frame.width, IS_IPHONE6 ? 440 : IS_IPHONE6PLUS ? 470 : 387)
        for textFieldObj in arrayTextFields{
            textFieldObj.setLeftPadding(20)
            textFieldObj.layer.cornerRadius = 3.0
        }
        buttonLogout.setTitle("¿¿Ya eres miembro?? registrarse", forState: UIControlState.Normal)
        if(Person.currentPerson().userId != nil) {
            loadData()
            buttonLogout.setTitle("Cerrar sesión", forState: UIControlState.Normal)

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- LOAD DATA
    func loadData (){
        arrayTextFields[0].text = Person.currentPerson().username
        arrayTextFields[1].text = Person.currentPerson().userEmail
        arrayTextFields[2].text = Person.currentPerson().userPassword
        ImageViewProfile.sd_setImageWithURL(NSURL(string: imagePath + Person.currentPerson().userImage), placeholderImage: UIImage(named: "ImagePlaceholder"))
    }

    //MARK:- BUTTON ACTIONS
    //MARK:- CHANGE IMAGE BUTTON ACTION
    @IBAction func onChangeImageButtonPressed (sender : UIButton){
        imagePicker = imagePicker == nil ? UIImagePickerController() : imagePicker
        imagePicker.view.backgroundColor = UIColor.whiteColor()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
        
    }
    
    //MARK:- SAVE PROFILE BUTTON ACTION
    @IBAction func onSaveProfileButtonPressed (sender : UIButton){
//         if(Person.currentPerson().userId == nil){
            if (!arrayTextFields[1].IsValidEmail()){
                showAlert(title: "Error", message: " Introduzca un correo electrónico válido", buttonTitle: "Ok")
            }else if (!verifyFields()){
                showAlert(title: "Error", message: "Rellene todos los campos", buttonTitle: "Ok")
            }else{
                createProfileWithJson()
            }
//         }else{
//             showAlert(title: "Error", message: "Edit Profile on progress", buttonTitle: "Ok")
//        }
    }
    
    //MARK:- LOGOUT BUTTON ACTION
    @IBAction func onLogoutButtonPressed (sender : UIButton){
        if(Person.currentPerson().userId == nil){
            Person.logout()
            AppDelegate.getAppdelegateInstance().didLogout()
        }else{
            SVProgressHUD.show()
            Person.logout({ (success, error) -> Void in
                if(success){
                    SVProgressHUD.dismiss()
                    Person.logout()
                    AppDelegate.getAppdelegateInstance().didLogout()
                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            })
        }
    }
    
    //MARK:- SHOW CUSTOM ALERT
    func showAlert(title title : String, message : String , buttonTitle : String){
        let alertView : BMAlert = (NSBundle.mainBundle().loadNibNamed("BMAlert", owner: self, options: nil) as NSArray)[0] as! BMAlert
        alertView.delegate = self
        alertView.configureView(title: title, message: message, buttonTitle: buttonTitle, labelAlignment: NSTextAlignment.Center)
        KGModal.sharedInstance().showWithContentView(alertView)
    }
    
    
    //MARK:- VERIFYING TEXTFIELD IS EMPTY OR NOT
    func verifyFields() -> Bool
    {
        var success : Bool = true
        for (var i = 0 ; i < 3 ; i++) {
            let textFieldObj : UITextField = arrayTextFields[i]
            if textFieldObj.text!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0
            {
                success = false
            }
        }
        return success
    }
    
    //MARK:- PROFILE CREATION WITH JSON
    func createProfileWithJson(){
        SVProgressHUD.show()
        var dict : NSDictionary!
        if(Person.currentPerson().userId == nil){
//            dict  = ["type" : "company_registeration","username" : arrayTextFields[0].text! , "email" : arrayTextFields[1].text! , "password" : arrayTextFields[2].text!, "address" : USERDICT["address"] as! String , "jobtype" : USERDICT["jobtype"] as! String ,"distance" : USERDICT["distance"] as! String]
            dict = ["user_name" : arrayTextFields[0].text! , "user_mail" : arrayTextFields[1].text! , "user_password" : arrayTextFields[2].text! , "user_address" : USERDICT["address"]! , "user_type" : "Company" , "experience1" : "" , "experience2" : "" , "experience3" : "" , "job_type" : USERDICT["jobtype"] as! String , "distance" : USERDICT["distance"] as! String , "deviceToken" : "" , "type" : "signup"]

        }else{
            //52.20.113.206/jobaloonNew/json/Api.php?type=editCompony&userid&user_name&user_email&user_password&user_address&chat_notification
         //   dict  = ["type" : "editCompony","user_name" : arrayTextFields[0].text! , "user_email" : arrayTextFields[1].text! , "user_password" : arrayTextFields[2].text!, "user_address" : Person.currentPerson().userAddress , "chat_notification" : "" ,"userid" : Person.currentPerson().userId]
            
            dict = ["userid" : Person.currentPerson().userId , "oauthToken" : Person.currentPerson().oauthToken ,"user_name" : arrayTextFields[0].text! , "user_email" : arrayTextFields[1].text! , "password" : arrayTextFields[2].text! , "user_address" : Person.currentPerson().userAddress , "user_type" : "Company" , "experience1" : "" , "experience2" : "" , "experience3" : "" ,  "type" : "editprofile"]

        }
        var imageArray : NSArray! = []
        if selectedImageData != nil{
            imageArray = [["value": selectedImageData! , "key" : "image"]]
        }
        Person.createCompanyProfile(Data: dict as NSDictionary, imageArray: imageArray) { (success, result, error) -> Void in
            if(success){
                SVProgressHUD.dismiss()
                self.navigationController?.popToRootViewControllerAnimated(true)
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
}

//MARK:- TEXTFIELD DELEGATE
extension CompanyRegistrationTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK:- GESTURE RECOGNISER DELEGATES
extension CompanyRegistrationTableViewController : UIGestureRecognizerDelegate{
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

//MARK:- IMAGEPICKER CONTROLLER DELEGATE
extension CompanyRegistrationTableViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
        var compression : CGFloat = 0.7
        let maxCompression : CGFloat = 0.1
        let maxFileSize : Int = 250*1024
        var imageData : NSData? = UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage] as! UIImage, compression)
        while (imageData?.length > maxFileSize && compression > maxCompression)
        {
            compression -= 0.1
            imageData = UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage] as! UIImage, compression)
        }
        selectedImageData = imageData!
        ImageViewProfile.image = UIImage(data: imageData!)
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

//MARK:- BMALERT DELEGATE
extension CompanyRegistrationTableViewController : BMAlertDelegate{
    
    func buttonInAlertPressed(sender: UIButton) {
        KGModal.sharedInstance().hideAnimated(true)
    }
}

