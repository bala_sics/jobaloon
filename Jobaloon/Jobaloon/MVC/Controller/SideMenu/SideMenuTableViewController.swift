//
//  SideMenuTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 7/17/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class SideMenuTableViewController: UITableViewController {
    
    @IBOutlet var constraintXForImageView : NSLayoutConstraint!
    var navigation : MainNavigationController!
    var controllers  : NSMutableArray = NSMutableArray()
    var userType : String!
    var arraySideMenu : NSArray!
    @IBOutlet var arrayLabels : [UILabel]!
    @IBOutlet var arrayConstraints : [NSLayoutConstraint]!
    @IBOutlet var imageViewProfile : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userType  = Person.currentPerson().userId == nil ? USERDICT["type"] as! String : Person.currentPerson().userType!
        if (userType == "Company"){
           // arraySideMenu = ["See jobseekers","Search settings","My profile","Post new job offer","My candidates"]
            arraySideMenu = ["Ver trabajadores","Nueva búsqueda","Mi perfil","Crear nueva oferta","Mis candidatos"]
            (arrayLabels as NSArray).enumerateObjectsUsingBlock({ (lbl, idx, stop) -> Void in
                (lbl as! UILabel).text = self.arraySideMenu[idx] as? String
            })
            (arrayConstraints as NSArray).enumerateObjectsUsingBlock({ (obj, idx, stop) -> Void in
                (obj as! NSLayoutConstraint).constant = 0.5
            })
        }else{
            imageViewProfile.layer.cornerRadius = 40.0
            imageViewProfile.clipsToBounds = true
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        constraintXForImageView.constant = (screenSize.width - 70)/2 - 40
        self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "NavigationBG")!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        // iOS 7
        if(self.tableView.respondsToSelector(Selector("setSeparatorInset:"))){
            self.tableView.separatorInset = UIEdgeInsetsZero
        }
        // iOS 8
        if(self.tableView.respondsToSelector(Selector("setLayoutMargins:"))){
            if #available(iOS 8.0, *) {
                self.tableView.layoutMargins = UIEdgeInsetsZero
            } else {
                // Fallback on earlier versions
            };
        }
        if(cell.respondsToSelector(Selector("setLayoutMargins:"))){
            if #available(iOS 8.0, *) {
                cell.layoutMargins = UIEdgeInsetsZero
            } else {
                // Fallback on earlier versions
            };
        }
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 166
        }
        return IS_IPHONE4 ? 59 : 77
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        navigation = AppDelegate.getAppdelegateInstance().containerSideMenu.centerViewController as! MainNavigationController
        controllers = NSMutableArray(array: navigation.viewControllers)
        
        if (userType == "Company"){
            switch (indexPath.row)
            {
            case 0 :
                break
            case 1:
                let offers : OffersViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OffersVC") as! OffersViewController
                gotoDestination(offers)
                break
            case 2:
                let jobTypeVC : JobTypeTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("JobTypeVC") as! JobTypeTableViewController
                jobTypeVC.fromSideMenu = "YES"
                gotoDestination(jobTypeVC)
                
                break
            case 3 :
                let profile : CompanyRegistrationTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CompanyRegistrationVC") as! CompanyRegistrationTableViewController
                gotoDestination(profile)
                break
            case 4 :
                 if(Person.currentPerson().userId != nil){
                let offer : CreateOfferTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CreateOfferVC") as! CreateOfferTableViewController
                gotoDestination(offer)
                 }
                break
            case 5 :
                if(Person.currentPerson().userId != nil){
                    let myOffer : MyOffersTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyOffersVC") as! MyOffersTableViewController
                    gotoDestination(myOffer)
                }

            default :
                break
            }
            
        }else{
            switch (indexPath.row)
            {
            case 0 :
                break
            case 1:
                let offers : OffersViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OffersVC") as! OffersViewController
                gotoDestination(offers)
                break
            case 2:
                let jobTypeVC : JobTypeTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("JobTypeVC") as! JobTypeTableViewController
                jobTypeVC.fromSideMenu = "YES"
                gotoDestination(jobTypeVC)
                
                break
            case 3 :
                let profile : RegistrationTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RegistrationVC") as! RegistrationTableViewController
                gotoDestination(profile)
                break
                
            case 4 :
                 if(Person.currentPerson().userId != nil){
                let compVC : MyCompaniesTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyCompanyVC") as! MyCompaniesTableViewController
                gotoDestination(compVC)
                 }
                break
            case 5 :
                if(Person.currentPerson().userId != nil){
                    let appVC : MyApplicationsTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyApplicationsVC") as! MyApplicationsTableViewController
                    gotoDestination(appVC)
                }
                break
            default :
                break
            }
            
        }
        
        self.menuContainerViewController.menuState = MFSideMenuStateClosed
    }
    
    func gotoDestination(destViewController:UIViewController){
        let currentViewController: UIViewController = navigation.topViewController!
        if(String.fromCString(class_getName(destViewController.dynamicType)) == String.fromCString(class_getName(currentViewController.dynamicType))!){
            
        }else{
            controllers.addObject(destViewController)
            navigation.viewControllers = controllers as NSArray as! [UIViewController]
        }
        
    }
    
}
