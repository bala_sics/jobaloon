//
//  ChatViewController.swift
//  Jobaloon
//
//  Created by Bala on 9/28/15.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,HPGrowingTextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate, UIGestureRecognizerDelegate,UITextViewDelegate,chatCellDelegate {
    
    var toUserId : String!
    var toUserName : String!
    var tableViewChat : UITableView!
    var messageTextView : HPGrowingTextView!
    var messageImageView : UIImageView!
    var selectedImage : UIImage!
    var containerView: UIView!
    var messageImageButton : UIButton!
    var sendButton : UIButton!
    var progressview : UIProgressView!
    var arrayChat : NSMutableArray = NSMutableArray()
    var indexCount : NSInteger!
    var refreshControl : UIRefreshControl!
    var lastMessageId : NSString!
    var imagePicker : UIImagePickerController!
    var tapGesture : UITapGestureRecognizer!
    var isCurrentView : Bool!
    
    
    
    //MARK:- LOAD VIEW
    override func loadView() {
        super.loadView()
        ///CONTAINER VIEW
        containerView = UIView(frame: CGRectMake(CGRectGetMinY(self.view.frame), CGRectGetHeight(self.view.frame) -  40, CGRectGetWidth(self.view.frame), 40))
        containerView.tintColor = UIColor.blackColor()
        containerView.backgroundColor = UIColor.blueColor()
        
        ///MESSAGE TEXTVIEW
        messageTextView = HPGrowingTextView(frame: CGRectMake( CGRectGetMinX(containerView.frame) + 36.0, 6.0, CGRectGetWidth(containerView.frame) - 55.0, 34.0))
        messageTextView.isScrollable = false;
        messageTextView.contentInset = UIEdgeInsetsMake(0.0, 5.0, 0.0, 5.0);
        messageTextView.minNumberOfLines = 1;
        messageTextView.maxNumberOfLines = 500;
        messageTextView.maxHeight = 120
        messageTextView.font = UIFont.systemFontOfSize(16);
        messageTextView.delegate = self;
        //messageTextView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5.0, 0.0, 5.0, 0.0);
        messageTextView.internalTextView.showsVerticalScrollIndicator = false
        messageTextView.backgroundColor = UIColor.clearColor();
        messageTextView.placeholder = "Enter message here";
        messageTextView.autoresizingMask = UIViewAutoresizing.FlexibleWidth;
        messageTextView.autoresizingMask = UIViewAutoresizing.FlexibleWidth;
        
        //BACKGROUND IMAGE FOR MESSAGE TOOLBAR
        let rawEntryBackground : UIImage = UIImage(named: "MessageEntryInputField")!
        let entryBackground = rawEntryBackground.stretchableImageWithLeftCapWidth(13, topCapHeight: 22)
        let entryImageView : UIImageView = UIImageView(image: entryBackground)
        entryImageView.frame = CGRectMake(CGRectGetMinX(containerView.frame) + 34.0, 0.0, CGRectGetWidth(containerView.frame) - 67.0 , 40);
        entryImageView.autoresizingMask = [UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleWidth];
        let rawBackground : UIImage = UIImage(named: "MessageEntryBackground")!
        let background : UIImage = rawBackground.stretchableImageWithLeftCapWidth(13, topCapHeight: 22)
        let imageView : UIImageView = UIImageView(image: background)
        imageView.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(containerView.frame), CGRectGetHeight(containerView.frame));
        imageView.autoresizingMask = [UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleWidth];
        
        ///SEND BUTTON
        sendButton = UIButton(type: UIButtonType.Custom)
        sendButton.frame = CGRectMake(CGRectGetWidth(containerView.frame) - 42.0, 8.0, 46.0, 27.0);
        sendButton.autoresizingMask = [UIViewAutoresizing.FlexibleTopMargin, UIViewAutoresizing.FlexibleLeftMargin]
        sendButton.addTarget(self, action: "sendButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        sendButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        sendButton.setTitle("Send", forState: UIControlState.Normal)
        sendButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        sendButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        
        //SELECT MESSAGE IMAGE BUTTON
        messageImageButton = UIButton(type: UIButtonType.Custom)
        messageImageButton.frame = CGRectMake(CGRectGetMinX(containerView.frame) + 4.0, 12.0, 27.0, 22.0);
        messageImageButton.autoresizingMask = [UIViewAutoresizing.FlexibleTopMargin, UIViewAutoresizing.FlexibleLeftMargin];
        messageImageButton.addTarget(self, action: "addPhotoButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        messageImageButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        messageImageButton.setImage(UIImage(named: "CameraIcon"), forState: UIControlState.Normal)
        
        messageImageView = UIImageView()
        messageImageView.frame = CGRectMake(CGRectGetMinX(containerView.frame) + 11.0 + CGRectGetWidth(messageImageButton.frame), 9.0, 22.0, 22.0)
        messageImageView.contentMode = UIViewContentMode.ScaleAspectFit
        messageImageView.hidden = true
        containerView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleTopMargin];
        
        //VIEW HIERARCHY
        containerView.addSubview(imageView)
        containerView.addSubview(entryImageView)
        containerView.addSubview(sendButton)
        containerView.addSubview(messageImageButton)
        containerView.addSubview(messageImageView)
        containerView.addSubview(messageTextView)
        self.view.addSubview(containerView)
        messageImageButton.hidden = true
    }
    //MARK:- VIEW DIDLOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        //BACKGROUND IMAGE
        let imageView : UIImageView = UIImageView(frame: UIScreen.mainScreen().bounds)
        imageView.image = UIImage(named: "TabBg")
        self.view.addSubview(imageView)
        
        //TABLEVIEW
        tableViewChat = UITableView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height - 105))
        tableViewChat.delegate = self
        tableViewChat.dataSource = self
        tableViewChat.separatorStyle = UITableViewCellSeparatorStyle.None
        tableViewChat.backgroundColor = UIColor.clearColor()
        self.view.addSubview(tableViewChat)
        self.view.bringSubviewToFront(containerView)
        setUI()
    }
    
    //MARK:- SET USER INTERFACE
    func setUI(){
        //NAVIGATION BAR TITLE
        self.navigationItem.title = toUserName
        
        //PROGRESS VIEW
        progressview = UIProgressView()
        progressview.setProgress(0, animated: false)
        progressview.progressViewStyle = UIProgressViewStyle.Bar
        
        //REFRESH CONTROL
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: "handleRefresh:", forControlEvents: UIControlEvents.ValueChanged)
        refreshControl.tintColor = UIColor.lightGrayColor()
        tableViewChat.addSubview(refreshControl)
        indexCount = 0;
        getPreviousMessage(indexCount)
    }
    
    //MARK:- HANDLE PULL TO REFRESH
    func handleRefresh (sender : UIRefreshControl){
        indexCount = indexCount + 10
        getPreviousMessage(indexCount)
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        isCurrentView = true
        self.getLatestMessage()
    }
    
    //MARK:- VIEW WILL DISAPPEAR
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        isCurrentView = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- GET PREVIOUS MESSAGES
    func getPreviousMessage (index : NSInteger){
        if(index == 0) { SVProgressHUD.showWithStatus("Processing...") }
        
        let params = ["type" : "listchat" , "userid" : Person.currentPerson().userId , "toId" : toUserId , "index" : "\(index)" , "oauthToken" : Person.currentPerson().oauthToken]
        Message.listMessage(params) { (success, result, error) -> Void in
            if(success){
                if((result as! NSArray).count > 0)
                {
                    if(index == 0) { self.arrayChat.removeAllObjects() }
                    self.arrayChat.insertObjects(result as! [AnyObject], atIndexes: NSIndexSet(indexesInRange: NSMakeRange(0, (result as! NSArray).count)))
                    self.tableViewChat.reloadData()
                    self.lastMessageId = self.arrayChat.lastObject?.valueForKey("messageId") as! NSString
                    self.getLatestMessage()
                    if(index == 0) {
                        self.tableScrollToBottom()
                        //                        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
                        //                            Int64(0.2 * Double(NSEC_PER_SEC)))
                        //                        dispatch_after(delayTime, dispatch_get_main_queue(), { () -> Void in
                        //                            self.tableScrollToBottom()
                        //                        })
                    }
                }
            }else{
                if(index == 0) { SVProgressHUD.showErrorWithStatus(error?.localizedDescription) }
            }
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
        }
    }
    
    //MARK:- GET LATEST MESSAGE
    func getLatestMessage (){
        
        if (lastMessageId == nil || !isCurrentView){
            return
        }
        
        let params = ["type" : "recentchat" , "userid" : Person.currentPerson().userId , "toId" : toUserId , "chatId" : lastMessageId , "oauthToken" : Person.currentPerson().oauthToken]
        Message.listMessage(params) { (success, result, error) -> Void in
            if(success){
                if((result as! NSArray).count > 0)
                {
                    let predicate : NSPredicate = NSPredicate(format: "messageId == %@", "NewMessage")
                    let arrayPredicate : NSArray = self.arrayChat.filteredArrayUsingPredicate(predicate)
                    self.arrayChat.removeObjectsInArray(arrayPredicate as [AnyObject])
                    self.arrayChat.insertObjects(result as! [AnyObject], atIndexes: NSIndexSet(indexesInRange: NSMakeRange(self.arrayChat.count, (result as! NSArray).count)))
                    self.tableViewChat.reloadData()
                    self.tableScrollToBottom()
                    self.lastMessageId = self.arrayChat.lastObject?.valueForKey("messageId") as! String
                }
            }else{
                print("Error in message \(error?.localizedDescription)")
            }
            let delayTime = dispatch_time(DISPATCH_TIME_NOW,
                Int64(1 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue(), { () -> Void in
                self.getLatestMessage()
            })
        }
    }
    
    //MARK:_ BUTTON ACTIONS
    //MARK:- SEND BUTTON ACTION
    func sendButtonAction (sender : UIButton){
      //  messageTextView.resignFirstResponder()
        if (messageTextView.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0 && messageImageView.image == nil)
        {
            messageTextView.text = ""
            return
        }
        self.view.insertSubview(progressview, aboveSubview: containerView)
        self.view.bringSubviewToFront(progressview)
        progressview.progress = 0.1
        let messg : Message = Message()
        messg.userId = Person.currentPerson().userId
        messg.messageText = messageTextView.text == nil ? "" : messageTextView.text
        messg.messageImage = messageImageView.image == nil ? nil : messageImageView.image
        messg.messageImageStr = ""
        messg.profileImageStr = Person.currentPerson().userImage
        messg.messageTime = NSDate.changeLocalTimeZoneToServerForDate()
        messg.messageId = "NewMessage"
        sendButton.enabled = false
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.progressview.progress = 1.0
            }) { (finished) -> Void in
                self.addMessage(messg)
        }
    }
    
    func addMessage (newMessage : Message){
        arrayChat.addObject(newMessage)
        tableViewChat.reloadData()
        progressview.removeFromSuperview()
        self.tableScrollToBottom()
        self.addmessageWithJson(newMessage)
        sendButton.enabled = true
        messageTextView.text = nil
        messageImageView.image = nil
        messageImageView.hidden = true
        messageTextView.frame = CGRectMake(CGRectGetMinX(containerView.frame) + 36.0, 6.0, CGRectGetWidth(containerView.frame) - 69.0, 34.0)
        tableViewChat.frame = CGRectMake(CGRectGetMinX(self.view.bounds), CGRectGetMinY(self.view.bounds) , CGRectGetWidth(self.view.bounds), (CGRectGetMinY(containerView.frame) - 3.0))
    }
    
    func addmessageWithJson(messg : Message){
        let params  = ["oauthToken" : Person.currentPerson().oauthToken , "userid" : Person.currentPerson().userId , "toId" : toUserId , "Message" : messg.messageText]
        Message.insertMessage(params, image: messg.messageImage) { (success, error) -> Void in
            
        }
    }
    
    //MARK:- CAMERA BUTTON ACTION
    func addPhotoButtonAction (sender : UIButton){
        messageTextView.resignFirstResponder()
        UIActionSheet(title: "Choose", delegate: self, cancelButtonTitle:nil , destructiveButtonTitle:nil, otherButtonTitles: "Camera", "PhotoAlbum","Cancel").showInView((self.view)!)
    }
    
    //MARK:- ACTIONSHEET DELEGATE
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        imagePicker = imagePicker == nil ? UIImagePickerController() : imagePicker
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        switch (buttonIndex)
        {
        case 0 :
            let hasCamera : Bool = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
            imagePicker.sourceType = hasCamera ? UIImagePickerControllerSourceType.Camera : UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(imagePicker, animated: true, completion: nil)
            break
        case 1 :
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(imagePicker, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
    //MARK:- IMAGEPICKERCONTROLLER DELEGATE
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.dismissViewControllerAnimated(true, completion: nil)
        var compression : CGFloat = 0.7
        let maxCompression : CGFloat = 0.1
        let maxFileSize : Int = 250*1024
        var imageData : NSData? = UIImageJPEGRepresentation(image, compression)
        while (imageData?.length > maxFileSize && compression > maxCompression)
        {
            compression -= 0.1
            imageData  = UIImageJPEGRepresentation(image, compression)
        }
        messageImageView.image = UIImage(data: imageData!)
        messageImageView.hidden = false
        let frame : CGRect = messageTextView.frame
        messageTextView.frame = CGRectMake(CGRectGetMinX(containerView.frame) + 58.0, 3.0, CGRectGetWidth(containerView.frame) - 91.0, CGRectGetHeight(frame))
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:- SCROLL TABLE TO BOTTOM
    func tableScrollToBottom(){
        if(arrayChat.count > 0){
            tableViewChat.scrollToRowAtIndexPath(NSIndexPath(forRow: arrayChat.count - 1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: false)
        }
    }
    
    //MARK:- TAP MESSAGE IMAGE
    func didTapImage (messageAttachedImage : UIImage){
        let contentView : UIView = UIView(frame: self.view.bounds)
        contentView.backgroundColor = UIColor.clearColor().colorWithAlphaComponent(0.75)
        if (messageAttachedImage.isKindOfClass(UIImage))
        {
            let imageviewModal : UIImageView = UIImageView(frame: contentView.frame)
            imageviewModal.contentMode = UIViewContentMode.ScaleAspectFit
            imageviewModal.image = messageAttachedImage
            contentView.addSubview(imageviewModal)
            UIView.transitionWithView(self.view, duration: 0.50, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
                self.view.addSubview(contentView)
                }, completion: nil)
            self.view.bringSubviewToFront(imageviewModal)
            
            let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "setUpTapGesture:")
            tapGesture.numberOfTouchesRequired = 1
            tapGesture.numberOfTapsRequired = 1
            contentView.addGestureRecognizer(tapGesture)
            tapGesture.delegate = self
        }
    }
    
    //MARK:- TAP GESTURE ACTION
    func setUpTapGesture(gestureRecognizer : UITapGestureRecognizer){
        UIView.transitionWithView(self.view, duration: 0.5, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
            gestureRecognizer.view!.removeFromSuperview()
            }, completion: nil)
    }
    
    
    // MARK: - TABLE VIEW DELEGATES
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChat.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = "cell"
        var cell : ChatTableViewCell? = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as? ChatTableViewCell
        if(cell == nil){
            cell = ChatTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: reuseIdentifier)
            cell?.cellDelegate = self
            cell?.backgroundColor = UIColor.clearColor()
            cell?.contentView.backgroundColor = UIColor.clearColor()
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
        }
        self.configureCell(cell!, indexpath: indexPath)
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        let margin : CGFloat = (15.0 + (2 * 12.5))
        let messg : Message = arrayChat[indexPath.row] as! Message
        
        let dateformatter : NSDateFormatter = NSDateFormatter()
        dateformatter.dateStyle = NSDateFormatterStyle.LongStyle
        let messageTime : NSString = NSString(format: "Delivered %@",messg.messageTime)
        
        let totalTextHeight : CGFloat = ChatTableViewCell.heightForLabel(messg.messageText.stringByRemovingPercentEncoding! , font: UIFont(name: "HelveticaNeue-Light", size: 17.0)!,width: ChatTableViewCell.maxTextWidth()).height + ChatTableViewCell.heightForLabel(messageTime as String , font: UIFont.boldSystemFontOfSize(12),width: ChatTableViewCell.maxTextWidth() + (2 * 17.5)).height
        let imageHeight : CGFloat = messg.messageImageStr.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0 && messg.messageImage == nil ? 0 : 115
        let totalCalculatedheight : CGFloat = margin + totalTextHeight + imageHeight
        
        return totalCalculatedheight > 70 ? totalCalculatedheight : 70
        
    }
    
    func configureCell (cell : ChatTableViewCell , indexpath : NSIndexPath){
        let messg : Message = arrayChat[indexpath.row] as! Message
        cell.isSent = messg.userId != Person.currentPerson().userId ? true : false
        cell.messageStatusLabel.textAlignment = cell.isSent == true ? NSTextAlignment.Left : NSTextAlignment.Right
        cell.messageLabel.textColor =  cell.isSent == true ? UIColor.blueColor() : UIColor.brownColor()
        cell.setNewMessage(messg)
    }
    
    //MARK:- TEXTVIEW DELEGATES
    func growingTextView(growingTextView: HPGrowingTextView!, willChangeHeight height : CGFloat) {
        let diff : CGFloat = (CGRectGetHeight(growingTextView.frame) - height as CGFloat) as CGFloat
        var r : CGRect = containerView.frame;
        r.size.height -= diff;
        r.origin.y += diff;
        if (r.size.height < 135){ containerView.frame = r }
    }
    
    //MARK:- KEYBOARD SHOW
    func keyboardWillShow (note : NSNotification){
        var info = note.userInfo!
        var keybounds : CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        keybounds = self.view.convertRect(keybounds, toView: nil)
        var containerFrame : CGRect = containerView.frame;
        containerFrame.origin.y = CGRectGetHeight(self.view.bounds) - (CGRectGetHeight(keybounds) + CGRectGetHeight(containerFrame))
        
        UIView.animateWithDuration((info[UIKeyboardAnimationDurationUserInfoKey] as! Double) - 0.01, delay: 0.0, options:UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
            self.containerView.frame = containerFrame
            self.progressview.frame = CGRectMake(0, CGRectGetMinY(self.containerView.frame) - 3.0, CGRectGetWidth(self.view.frame), 3.0)
            self.tableViewChat.frame = CGRectMake(CGRectGetMinX(self.view.bounds), CGRectGetMinY(self.view.bounds) - 10.0, CGRectGetWidth(self.view.bounds), CGRectGetMinY(self.progressview.frame) + 23.0)
            }) { (success) -> Void in
                
                
                
        }
        tapGesture = UITapGestureRecognizer(target: self, action: "onTableViewTapped")
        tapGesture.numberOfTapsRequired = 1
        tableViewChat.addGestureRecognizer(tapGesture)
        tableScrollToBottom()
    }
    
    //MARK:- KEYBOARD HIDE
    func keyboardWillHide (note : NSNotification){
        var containerFrame : CGRect = containerView.frame;
        var info = note.userInfo!
        containerFrame.origin.y = CGRectGetHeight(self.view.bounds) - CGRectGetHeight(containerFrame)
        
        UIView.animateWithDuration((info[UIKeyboardAnimationDurationUserInfoKey] as! Double), delay: 0.0, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
            self.containerView.frame = containerFrame;
            self.progressview.frame = CGRectMake(0.0, CGRectGetMinY(self.containerView.frame) - 3.0, CGRectGetWidth(self.view.frame), 3.0)
            self.tableViewChat.frame = CGRectMake(CGRectGetMinX(self.view.bounds), CGRectGetMinY(self.view.bounds) , CGRectGetWidth(self.view.bounds), CGRectGetMinY(self.progressview.frame) + 5)
            
            }, completion: nil)
        if(tapGesture != nil) { tableViewChat.removeGestureRecognizer(tapGesture) }
    }
    
    //MARK:- TAPGESTURE ACTION
    func onTableViewTapped (){
        messageTextView.text = nil;
        messageTextView.resignFirstResponder()
        
    }
    
}
