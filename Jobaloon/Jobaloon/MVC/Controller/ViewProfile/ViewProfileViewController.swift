//
//  ViewProfileViewController.swift
//  Jobaloon
//
//  Created by Bala on 09/10/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class ViewProfileViewController: UIViewController {

    var candidateObj : MyCandidate!
    var buttonLike : UIButton!
    var buttonDislike : UIButton!
     var  viewUser : OfferView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = candidateObj.candidateUsername
        self.view.backgroundColor = UIColor(red: 241.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        setUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        viewUser.stopPlayingVideo()
    }

    func setUI (){
        
        buttonLike = UIButton(type: UIButtonType.Custom)
        buttonLike.frame = CGRectMake(5, screenSize.height/2 - 90, 60, 60)
        buttonLike.setImage(UIImage(named: "DislikeButton"), forState: UIControlState.Normal)
        buttonLike.addTarget(self, action: "likeButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        buttonDislike = UIButton(type: UIButtonType.Custom)
        buttonDislike.frame = CGRectMake(screenSize.width - 65, screenSize.height/2 - 90, 60, 60)
        buttonDislike.setImage(UIImage(named: "LikeButton"), forState: UIControlState.Normal)
        buttonDislike.addTarget(self, action: "dislikeButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)

        viewUser = OfferView(frame: CGRectMake(30, 30, screenSize.width - 60, screenSize.height - 120))
        viewUser.backgroundColor = UIColor.whiteColor()
        viewUser.configureProfileView(candidateObj)
        self.view.addSubview(viewUser)
        self.view.addSubview(buttonLike)
        self.view.addSubview(buttonDislike)

        buttonLike.hidden = candidateObj.candidateCompanyStatus != "Not seen"
        buttonDislike.hidden = candidateObj.candidateCompanyStatus != "Not seen"

    }
 
    //MARK:- BUTTON ACTION
    //MARK:- LIKE BUTTON ACTION
    func likeButtonPressed (sender : UIButton){
        likeOrDislikeOfferWithJson(status: "Dislike")
    }
    
    //MARK:- DISLIKE BUTTON ACTION
    func dislikeButtonPressed (sender : UIButton){
        likeOrDislikeOfferWithJson(status: "Like")

    }

    //MARK:- LIKE OR DISLIKE OFFER WITH JSON
    func likeOrDislikeOfferWithJson (status status : String){
        
              SVProgressHUD.show()
            Offer.JobSeekers.likeOrDislikeJobSeekers(jobSeekerId: candidateObj.candidateUserId, likestatus: status, completion: { (success, error) -> Void in
                if (success){
                    SVProgressHUD.dismiss()
                    self.buttonDislike.hidden = true
                    self.buttonLike.hidden = true
                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)

                }
            })
    }

}
