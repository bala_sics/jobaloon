//
//  MyCompaniesTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 9/28/15.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MyCompaniesTableViewController: UITableViewController {

    var arrayCompanies : NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Chat"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        getMyApplications()
    }

    //MARK:- GET MY APPLICATIONS FROM JSON
    func getMyApplications (){
        SVProgressHUD.show()
        MyApplication.getMyCompanies { (success, result, error) -> Void in
            if(success){
                self.arrayCompanies.removeAllObjects()
                self.arrayCompanies.addObjectsFromArray(result as! [AnyObject])
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }else{
                error?.localizedDescription == "No Companies found" ? SVProgressHUD.dismiss() : SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
                self.tableView.scrollEnabled = false
                let label : UILabel = UILabel(frame: CGRectMake(0, screenSize.height/2 - 60,screenSize.width , 60))
                label.text = "No chats in history"
                label.textAlignment = NSTextAlignment.Center
                label.textColor = UIColor.blackColor()
                self.view.addSubview(label)
            }
        }
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCompanies.count
    }
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        // iOS 7
        if(self.tableView.respondsToSelector(Selector("setSeparatorInset:"))){
            self.tableView.separatorInset = UIEdgeInsetsZero
        }
        // iOS 8
        if(self.tableView.respondsToSelector(Selector("setLayoutMargins:"))){
            if #available(iOS 8.0, *) {
                self.tableView.layoutMargins = UIEdgeInsetsZero
            } else {
                // Fallback on earlier versions
            };
        }
        if(cell.respondsToSelector(Selector("setLayoutMargins:"))){
            if #available(iOS 8.0, *) {
                cell.layoutMargins = UIEdgeInsetsZero
            } else {
                // Fallback on earlier versions
            };
        }
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 75
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell  = tableView.dequeueReusableCellWithIdentifier("cel")
        if (cell == nil){
            cell = MyCompanyTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            
        }
        (cell as! MyCompanyTableViewCell).configureCell(arrayCompanies[indexPath.row] as! MyApplication.CompanyDetails)
        return cell!
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let app : MyApplication.CompanyDetails = arrayCompanies[indexPath.row] as! MyApplication.CompanyDetails
        let chat : ChatViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ChatVC") as! ChatViewController
        chat.toUserId = app.userId
        chat.toUserName = app.username
        self.navigationController?.pushViewController(chat, animated: true)
    }
}
