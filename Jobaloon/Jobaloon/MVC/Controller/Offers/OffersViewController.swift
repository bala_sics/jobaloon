//
//  OffersViewController.swift
//  Jobaloon
//
//  Created by Bala on 7/9/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit
import MediaPlayer

class OffersViewController: UIViewController,BMAlertDelegate,UIGestureRecognizerDelegate {
    
    var viewPlaceHolder : OfferView!
    var centerPoint : CGPoint = CGPoint()
    var viewToAnimatePresent : OfferView!
    var viewToAnimatePrevious : OfferView!
    var buttonLike : UIButton!
    var buttonDislike : UIButton!
    var indexCount : NSInteger = 0
    var removedViewsCount : NSInteger = 0
    var arrayJobOffers : NSMutableArray = NSMutableArray()
    var userType : String!
    var address : String!
    var jobtype : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Ver ofertas"
        self.view.backgroundColor = UIColor(red: 241.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        createPlaceholderView()
         userType  = Person.currentPerson().userId == nil ? USERDICT["type"] as! String : Person.currentPerson().userType!
        userType == "Company" ? getJobSeekers() : getJobOffers()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "profileCreatedSuccessfully", name: "ProfileCreated", object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        if(self.viewToAnimatePresent != nil){
            self.viewToAnimatePresent.stopPlayingVideo()
        }
    }
    
    //MARK:- BUTTON ACTION
    //MARK:- LIKE BUTTON ACTION
    func likeButtonPressed (sender : UIButton){
        if(Person.currentPerson().userId == nil){
            showAlert(title: "¿Quieres inscribirte a nuestras ofertas?", message: "Crea un perfil para que las empresas puedan seleccionarte", buttonTitle: "Crear perfil" ,alignment : NSTextAlignment.Center)
        }else if (userType == "Company" && Int(Person.currentPerson().offerCount) == 0){
            showAlert(title: "Alert", message: "Please create a new offer to see jobseekers", buttonTitle: "Create offer" ,alignment : NSTextAlignment.Center)
        }else{
            if(arrayJobOffers.count > 0 && arrayJobOffers.count != removedViewsCount) { likeAnimation() }
        }
    }
    
    //MARK:- DISLIKE BUTTON ACTION
    func dislikeButtonPressed (sender : UIButton){
        if(Person.currentPerson().userId == nil){
            showAlert(title: "¿Quieres inscribirte a nuestras ofertas?", message: "Crea un perfil para que las empresas puedan seleccionarte", buttonTitle: "Crear perfil" ,alignment : NSTextAlignment.Center)
        }else if (userType == "Company" && Int(Person.currentPerson().offerCount) == 0){
            showAlert(title: "Alert", message: "Please create a new offer to see jobseekers", buttonTitle: "Create offer" ,alignment : NSTextAlignment.Center)
        }else{
            if(arrayJobOffers.count > 0 && arrayJobOffers.count != removedViewsCount) {  disLikeAnimation() }
        }
    }
    
    //MARK:- BMALERT DELEGATE
    func buttonInAlertPressed(sender: UIButton) {
        KGModal.sharedInstance().hideAnimated(true)
        if(sender.titleLabel?.text == "Crear perfil"){
            if(USERDICT["type"] as! String == "Company")
            {
                let reg : CompanyRegistrationTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CompanyRegistrationVC") as! CompanyRegistrationTableViewController
                self.navigationController?.pushViewController(reg, animated: true)

            }else{
                let reg : RegistrationTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RegistrationVC") as! RegistrationTableViewController
                self.navigationController?.pushViewController(reg, animated: true)
            }
        }else if(sender.titleLabel?.text == "Create offer"){
            let offer : CreateOfferTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CreateOfferVC") as! CreateOfferTableViewController
            self.navigationController?.pushViewController(offer, animated: true)

        }
    }
    
    //MARK:- CREATE PROFILE NOTIFICATION
    func profileCreatedSuccessfully() {
        showAlert(title: "¡Ahora puedes inscribirte a nuestras ofertas!", message: "1. Selecciona Ofertas para ver las ofertas disponibles. \n2. Selecciona Ajustes para cambiar los criterios de búsqueda. \n3. Selecciona Mi perfil para cambiar tu vídeo y descripción. \n4. Selecciona Chat para enviar mensajes a las empresas interesadas en ti. \n5. Selecciona Inscripciones para ver tu estatus en todas las ofertas que te has inscrito.", buttonTitle: "Ok" ,alignment : NSTextAlignment.Left)
    }
    
    //MARK:- SHOW CUSTOM ALERT
    func showAlert(title title : String, message : String , buttonTitle : String ,alignment : NSTextAlignment){
        let alertView : BMAlert = (NSBundle.mainBundle().loadNibNamed("BMAlert", owner: self, options: nil) as NSArray)[0] as! BMAlert
        alertView.delegate = self
        alertView.configureView(title: title, message: message, buttonTitle: buttonTitle, labelAlignment: alignment)
        KGModal.sharedInstance().showWithContentView(alertView)
    }
    
    //MARK:- GET JOB OFFERS
    func getJobOffers(){
        SVProgressHUD.show()
        let jobType1 : String = jobtype != nil ? jobtype : Person.currentPerson().userId == nil ? USERDICT["jobtype"] as! String : Person.currentPerson().userLabels!
        let address1 : String = address != nil ? address : Person.currentPerson().userId == nil ? USERDICT["address"] as! String : Person.currentPerson().userAddress!
        
        Offer.getJobOffers(jobType: jobType1, address: address1) { (success, result, error) -> Void in
            if(success){
                self.arrayJobOffers.removeAllObjects()
                self.arrayJobOffers.addObjectsFromArray(result as! [AnyObject])
                self.createOfferView()
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
            self.buttonDislike.hidden = self.arrayJobOffers.count == 0
            self.buttonLike.hidden = self.arrayJobOffers.count == 0
        }
    }
    
    //MARK:- GET JOB SEEKERS
    func getJobSeekers (){
        SVProgressHUD.show()
        let distance : String = Person.currentPerson().userId == nil ? USERDICT["distance"] as! String : Person.currentPerson().userDistance == "" ? "10" : Person.currentPerson().userDistance
        let jobType1 : String = jobtype != nil ? jobtype : Person.currentPerson().userId == nil ? USERDICT["jobtype"] as! String : Person.currentPerson().userLabels!
        let address1 : String = address != nil ? address : Person.currentPerson().userId == nil ? USERDICT["address"] as! String : Person.currentPerson().userAddress!
        
        let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken == nil ? "" : Person.currentPerson().oauthToken , "job_type" : jobType1 , "location" : address1 , "userid" : Person.currentPerson().userId == nil ? "" : Person.currentPerson().userId , "distance" : distance]

        Offer.JobSeekers.getJobSeekers(data: params) { (success, result, error) -> Void in
            if(success){
                self.arrayJobOffers.removeAllObjects()
                self.arrayJobOffers.addObjectsFromArray(result as! [AnyObject])
                self.createOfferView()
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
            self.buttonDislike.hidden = self.arrayJobOffers.count == 0
            self.buttonLike.hidden = self.arrayJobOffers.count == 0
        }
    }
    
    //MARK:- LIKE OR DISLIKE OFFER WITH JSON
    func likeOrDislikeOfferWithJson (status status : String , offerId : String){
        if(Person.currentPerson().userId == nil) {
            return
        }

        if (userType == "Company"){
            Offer.JobSeekers.likeOrDislikeJobSeekers(jobSeekerId: offerId, likestatus: status, completion: { (success, error) -> Void in
                if(error != nil){
                    print(" error :\(error?.localizedDescription)")
                }
            })
        }else{
            Offer.likeOrDislikeJobOffers(jobId:offerId, likestatus: status) { (success, error) -> Void in
                if(error != nil){
                    print(" error :\(error?.localizedDescription)")
                }
            }
        }
    }
    
    //MARK:- CREATE PLACEHOLDER VIEW
    func createPlaceholderView(){
        buttonLike = UIButton(type: UIButtonType.Custom)
        buttonLike.frame = CGRectMake(5, screenSize.height/2 - 90, 60, 60)
        buttonLike.setImage(UIImage(named: "DislikeButton"), forState: UIControlState.Normal)
        buttonLike.addTarget(self, action: "likeButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        buttonDislike = UIButton(type: UIButtonType.Custom)
        buttonDislike.frame = CGRectMake(screenSize.width - 65, screenSize.height/2 - 90, 60, 60)
        buttonDislike.setImage(UIImage(named: "LikeButton"), forState: UIControlState.Normal)
        buttonDislike.addTarget(self, action: "dislikeButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        //PLACEHOLDER VIEW
        viewPlaceHolder = OfferView(frame: CGRectMake(30, 30, screenSize.width - 60, screenSize.height - 120))
        viewPlaceHolder.backgroundColor = UIColor.whiteColor()
        viewPlaceHolder.configureNoOfferPage()
        self.view.addSubview(viewPlaceHolder)
        self.view.addSubview(buttonLike)
        self.view.addSubview(buttonDislike)
    }
    
    //MARK:- SWIPEABLE VIEWS
    func createOfferView(){
        if(arrayJobOffers.count > 0){
            viewToAnimatePresent = OfferView(frame: CGRectMake(30, 30, screenSize.width - 60, screenSize.height - 120))
            self.view.insertSubview(viewToAnimatePresent, aboveSubview: viewPlaceHolder)
            centerPoint = viewToAnimatePresent.center
            let panGesture : UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action:"moveAnimation:")
            panGesture.minimumNumberOfTouches = 1
            panGesture.delegate = self
            viewToAnimatePresent.addGestureRecognizer(panGesture)
            userType == "Company" ? viewToAnimatePresent.configureJobSeekerView(arrayJobOffers[0] as! Offer.JobSeekers) :  viewToAnimatePresent.configureView(arrayJobOffers[0] as! Offer)
//            if (userType == "Company") {
//                viewToAnimatePresent.startPlayingVideo()
//            }
        }
        
        if(arrayJobOffers.count > 1){
            viewToAnimatePrevious = OfferView(frame: CGRectMake(30, 30, screenSize.width - 60, screenSize.height - 120))
            self.indexCount = self.indexCount + 1
            self.view.insertSubview(viewToAnimatePrevious, aboveSubview: viewPlaceHolder)
            userType == "Company" ? viewToAnimatePrevious.configureJobSeekerView(arrayJobOffers[1] as! Offer.JobSeekers) :  viewToAnimatePrevious.configureView(arrayJobOffers[1] as! Offer)
        }
    }
    
    func createNextView (){
        if(viewToAnimatePrevious == nil) {
            return
        }
        viewToAnimatePresent = viewToAnimatePrevious
        let panGesture1 : UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action:"moveAnimation:")
        panGesture1.minimumNumberOfTouches = 1
        panGesture1.delegate = self
        viewToAnimatePresent.addGestureRecognizer(panGesture1)
        
        if(self.arrayJobOffers.count > self.indexCount){
            viewToAnimatePrevious = OfferView(frame: CGRectMake(30, 30, screenSize.width - 60, screenSize.height - 120))
            self.view.insertSubview(viewToAnimatePrevious, aboveSubview: viewPlaceHolder)
            userType == "Company" ? viewToAnimatePrevious.configureJobSeekerView(arrayJobOffers[indexCount] as! Offer.JobSeekers) :  viewToAnimatePrevious.configureView(arrayJobOffers[indexCount] as! Offer)
            if (self.userType == "Company") { viewToAnimatePrevious.stopPlayingVideo() }
        }
    }
    
    //MARK:- PAN GESTURE ACTION
    func moveAnimation (gestureRecognizer : UIPanGestureRecognizer)
    {
        
        let translation  = gestureRecognizer.translationInView(self.view)
        gestureRecognizer.view?.center = CGPointMake(gestureRecognizer.view!.center.x + translation.x, gestureRecognizer.view!.center.y)
        gestureRecognizer.setTranslation(CGPointZero, inView: self.view)
        
        ///GESTURE STATE CHANGED
        if gestureRecognizer.state == UIGestureRecognizerState.Changed
        {
            let velocity = gestureRecognizer.velocityInView(self.view)
            let transform : CGAffineTransform = velocity.x > 0 ? CGAffineTransformMakeRotation(-270) : CGAffineTransformMakeRotation(270)
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.viewToAnimatePresent.transform = transform
                }, completion: { (stop) -> Void in
            })
        }
        
        ///GESTURE STATE CANCELLED
        if gestureRecognizer.state == UIGestureRecognizerState.Cancelled
        {
            gestureRecognizer.view?.center = centerPoint
        }
        
        ///GESTURE STATE ENDED
        if gestureRecognizer.state == UIGestureRecognizerState.Ended
        {
            if gestureRecognizer.view?.center.x > centerPoint.x-100 && gestureRecognizer.view?.center.x < centerPoint.x+100
            {
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    gestureRecognizer.view?.center = self.centerPoint
                    let transform : CGAffineTransform = CGAffineTransformMakeRotation(0)
                    gestureRecognizer.view?.transform = transform
                    }, completion: { (stop) -> Void in
                })
            }
            else
            {
                let velocity = gestureRecognizer.velocityInView(self.view)
                if(Person.currentPerson().userId == nil){
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        gestureRecognizer.view?.center = self.centerPoint
                        let transform : CGAffineTransform = CGAffineTransformMakeRotation(0)
                        gestureRecognizer.view?.transform = transform
                        }, completion: { (stop) -> Void in
                    })
                    showAlert(title: "¿Quieres inscribirte a nuestras ofertas?", message: "Crea un perfil para que las empresas puedan seleccionarte", buttonTitle: "Crear perfil" ,alignment : NSTextAlignment.Center)
                }else if (userType == "Company" && Int(Person.currentPerson().offerCount) == 0){
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        gestureRecognizer.view?.center = self.centerPoint
                        let transform : CGAffineTransform = CGAffineTransformMakeRotation(0)
                        gestureRecognizer.view?.transform = transform
                        }, completion: { (stop) -> Void in
                    })
                    showAlert(title: "Alert", message: "Please create a new offer to see jobseekers", buttonTitle: "Create offer" ,alignment : NSTextAlignment.Center)
                }else{
                    velocity.x > 0 ?  self.disLikeAnimation() :  self.likeAnimation()
                }
            }
        }
    }
    
    //MARK:- LIKE ANIMATION
    func likeAnimation (){
        if (self.userType == "Company") { self.viewToAnimatePresent.stopPlayingVideo() }
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.likeOrDislikeOfferWithJson(status: "Dislike", offerId: self.userType == "Company" ? self.viewToAnimatePresent.jobseekerCopy.userId : self.viewToAnimatePresent.offerCopy.offerId)
            self.viewToAnimatePresent.center = CGPointMake(-400, self.centerPoint.y)
            self.viewToAnimatePresent.layer.transform = CATransform3DMakeRotation(-3.14/3, 0.0, 0.0, 1.0)
            },
            completion: { (stop) -> Void in
                self.removedViewsCount = self.removedViewsCount + 1
                self.viewToAnimatePresent.removeFromSuperview()
                self.indexCount = self.indexCount + 1
//                if (self.userType == "Company" && self.viewToAnimatePrevious != nil && self.removedViewsCount < self.arrayJobOffers.count) {
//                    self.viewToAnimatePrevious.startPlayingVideo()
//                }
                self.createNextView()
                self.buttonDislike.hidden = self.arrayJobOffers.count == self.removedViewsCount
                self.buttonLike.hidden = self.arrayJobOffers.count == self.removedViewsCount
        })
    }
    
    //MARK:- DISLIKE ANIMATION
    func disLikeAnimation (){
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            if (self.userType == "Company") {   self.viewToAnimatePresent.stopPlayingVideo() }
            self.likeOrDislikeOfferWithJson(status: "Like", offerId: self.userType == "Company" ? self.viewToAnimatePresent.jobseekerCopy.userId : self.viewToAnimatePresent.offerCopy.offerId)
            self.viewToAnimatePresent.center = CGPointMake(800, self.centerPoint.y)
            self.viewToAnimatePresent.layer.transform = CATransform3DMakeRotation(3.14/3, 0.0, 0.0, 1.0)
            },
            completion: { (stop) -> Void in
                self.removedViewsCount = self.removedViewsCount + 1
                self.viewToAnimatePresent.removeFromSuperview()
                self.indexCount = self.indexCount + 1
//                if (self.userType == "Company" && self.viewToAnimatePrevious != nil && self.removedViewsCount < self.arrayJobOffers.count) {
//                    self.viewToAnimatePrevious.startPlayingVideo()
//                }
                self.createNextView()
                self.buttonDislike.hidden = self.arrayJobOffers.count == self.removedViewsCount
                self.buttonLike.hidden = self.arrayJobOffers.count == self.removedViewsCount
        })
    }
    
    //MARK:- GESTURE RECOGNIZER DELEGATE
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool
    {
        return true
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
    
}
