//
//  CreateOfferTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 10/1/15.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class CreateOfferTableViewController: UITableViewController {
    
    @IBOutlet var arrayTextfields : [UITextField]!
    @IBOutlet var arrayButtons : [UIButton]!
    @IBOutlet var textviewDescription : GCPlaceholderTextView!
    @IBOutlet var datePicker : UIDatePicker!
    @IBOutlet var buttonDropDown : UIButton!
    var activeTextField : UITextField!
    @IBOutlet var viewToolbar : UIToolbar!
    var arrayHeights : NSMutableArray = NSMutableArray()
    var arrayJobs : NSMutableArray = NSMutableArray()
    var dropDown : BMDropDown!
    var arraySelectedDays : NSMutableArray = NSMutableArray()
    var searchQuery : SPGooglePlacesAutocompleteQuery!
    var placeView : BMGooglePlaces?
    var arrayLocations : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Nueva oferta"
        self.tableView.backgroundColor = UIColor(red: 241.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        arrayHeights = [60,60,60,87,60,117]
        searchQuery = SPGooglePlacesAutocompleteQuery(apiKey: GOOGLE_APIKEY)
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(0.2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue(), { () -> Void in
            self.setUI()
            self.getJobType()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- SET UI
    func setUI (){
        for textFieldObj in arrayTextfields{
            textFieldObj.setLeftPadding(10)
            textFieldObj.layer.cornerRadius = 3.0
            textFieldObj.setValue(UIColor.grayColor(), forKeyPath: "_placeholderLabel.textColor")
        }
        for buttons in arrayButtons{
            buttons.layer.cornerRadius = buttons.frame.width/2
            buttons.clipsToBounds = true
            buttons.layer.borderWidth = 1.0
            buttons.layer.borderColor = UIColor(red: 39.0/255.0, green: 146.0/255.0, blue: 200.0/255.0, alpha: 1.0).CGColor
        }
        textviewDescription.layer.cornerRadius = 3.0
        textviewDescription.placeholder = " Descripción de la oferta (140 car.)"
        textviewDescription.placeholderColor = UIColor.grayColor()
        
        ///SETTING INPUT VIEWS FOR TEXTFIELDS
        arrayTextfields[2].inputView = datePicker
        arrayTextfields[3].inputView = datePicker
        arrayTextfields[2].inputAccessoryView = viewToolbar
        arrayTextfields[3].inputAccessoryView = viewToolbar
        datePicker.locale = NSLocale(localeIdentifier: "da_DK")
        
        ////SETTING RIGHT PADDING VIEW
        arrayTextfields[1].rightViewMode = UITextFieldViewMode.Always
        let imgVw = UIImageView(frame: CGRectMake(0, 10, 20, 15))
        imgVw.image = UIImage(named: "DropDown")
        let paddingView = UIView(frame: CGRectMake(0, 0, 30, 30))
        paddingView.addSubview(imgVw)
        arrayTextfields[1].rightView = paddingView
        
    }
    
    //MARK:- CLEAR DATAS
    func clearDatas (){
        for textFieldObj in arrayTextfields{
            textFieldObj.text = ""
        }
        for buttons in arrayButtons{
            buttons.selected = false
            buttons.backgroundColor =  UIColor.clearColor()
            
        }
        textviewDescription.text = ""
        textviewDescription.placeholderColor = UIColor.grayColor()
    }
    
    //MARK:- BUTTON ACTIONS
    @IBAction func onDaysButtonSelected (sender : UIButton){
        if(!sender.selected){
            sender.backgroundColor =  UIColor(red: 39.0/255.0, green: 146.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        }else{
            sender.backgroundColor =  UIColor.clearColor()
            
        }
        sender.selected = !sender.selected
        if (arraySelectedDays.containsObject(sender.titleLabel!.text!)){
            arraySelectedDays.removeObject(sender.titleLabel!.text!)
        }else{
            arraySelectedDays.addObject(sender.titleLabel!.text!)
        }
    }
    
    //MARK:- DATEPICKER VALUE CHANGED
    @IBAction func datepickerValueChanged (sender : UIDatePicker){
        let dateFormat = NSDateFormatter()
        dateFormat.timeZone = NSTimeZone.systemTimeZone()
        dateFormat.dateFormat = "HH:mm"
        activeTextField.text = dateFormat.stringFromDate(datePicker.date)
    }
    
    //MARK:- DONE TOOLBAR BUTTON ACTION
    @IBAction func onDoneButtonPressed (sender : UIDatePicker){
        activeTextField.resignFirstResponder()
    }
    
    @IBAction func onPostOfferButtonPressed (sender : UIButton){
        if (arrayTextfields[0].text == "" || arrayTextfields[1].text == "" || arrayTextfields[4].text == "" || textviewDescription.text == ""){
            showAlert("Please fill the required fields")
        }else{
            createOfferWithJson()
        }
    }
    
    //MARK:- DROPDOWN BUTTON ACTION
    @IBAction func onDropDownPressed (sender : UIButton){
        if(activeTextField != nil) { activeTextField.resignFirstResponder() }
        
        if (!sender.selected){
            arrayHeights.replaceObjectAtIndex(1, withObject: 250)
            dropDown = BMDropDown(frame: CGRectMake(sender.frame.origin.x, CGRectGetMaxY(sender.frame), CGRectGetWidth(sender.frame), 176))
            dropDown.delegate = self
            dropDown.arrayToLoad = self.arrayJobs.mutableCopy() as! NSMutableArray
            sender.superview?.addSubview(dropDown)
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            sender.selected = true
        }else{
            dropDown.hideMenu()
        }
    }
    
    //MARK;- TABLEVIEW DELEGATES
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return arrayHeights[indexPath.row] as! CGFloat
    }
    
    //MARK:- GET JOB TYPE
    func getJobType(){
        SVProgressHUD.show()
        Person.getJobTypes { (success, result, error) -> Void in
            if(success){
                let array : NSArray = result as! NSArray
                let sortedArray : NSArray = array.sortedArrayUsingSelector("localizedCaseInsensitiveCompare:")
                self.arrayJobs.addObjectsFromArray(sortedArray as [AnyObject])
                SVProgressHUD.dismiss()
            }else{
                self.getJobType()
               // SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    //MARK:- SHOW ALERT
    func showAlert (text : String){
        let alertView : BMAlert = (NSBundle.mainBundle().loadNibNamed("BMAlert", owner: self, options: nil) as NSArray)[0] as! BMAlert
        alertView.delegate = self
        alertView.configureView(title: "Error", message: text, buttonTitle: "Ok", labelAlignment: NSTextAlignment.Center)
        KGModal.sharedInstance().showWithContentView(alertView)
    }
    
    //MARK:- CREATE OFFER WITH JSON
    func createOfferWithJson(){
        SVProgressHUD.show()
        let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken,"Role" : arrayTextfields[1].text! , "start_date" : arrayTextfields[2].text! , "end_date" : arrayTextfields[3].text! ,"working_days" : arraySelectedDays.componentsJoinedByString(",") ,"location" : arrayTextfields[4].text! , "jobDescription" : textviewDescription.text , "userid" : Person.currentPerson().userId , "offer_name" : arrayTextfields[0].text!]
        Offer.createJobOffers(data: params) { (success, error) -> Void in
            if(success){
                self.clearDatas()
                Person.currentPerson().offerCount = "1"
                let myOffer : MyOffersTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyOffersVC") as! MyOffersTableViewController
                let navigation : MainNavigationController = AppDelegate.getAppdelegateInstance().containerSideMenu.centerViewController as! MainNavigationController
                let controllers : NSMutableArray = NSMutableArray(array: navigation.viewControllers)
                controllers.addObject(myOffer)
                navigation.viewControllers = controllers as NSArray as! [UIViewController]
                SVProgressHUD.showSuccessWithStatus("Offer created successfully")
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    //MARK:- GEOLOCATION
    func getLocation(searchText : String){
        searchQuery.input = searchText
        searchQuery.fetchPlaces { (places, error) -> Void in
            if (error != nil) {
                print("error")
                SVProgressHUD.showErrorWithStatus(error.localizedDescription)
            }else{
                self.arrayLocations.removeAllObjects()
                self.arrayLocations.addObjectsFromArray(places  as [AnyObject])
                if(self.placeView == nil){
                    self.placeView = BMGooglePlaces(frame: CGRectMake(CGRectGetMinX(self.arrayTextfields[4].frame), CGRectGetMaxY(self.arrayTextfields[4].frame), CGRectGetWidth(self.arrayTextfields[4].frame), 88))
                    self.placeView?.delegate = self
                    self.placeView!.arrayToLoad = self.arrayLocations.mutableCopy() as! NSMutableArray
                    self.arrayTextfields[4].superview!.addSubview(self.placeView!)
                    self.arrayHeights.replaceObjectAtIndex(4, withObject: 150)
                    self.tableView.beginUpdates()
                    self.tableView.endUpdates()
                }else{
                    self.placeView?.reloadPlaces(self.arrayLocations)
                }
            }
        }
    }
    
}

//MARK:- TEXTFIELD DELEGATE
extension CreateOfferTableViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if(textField.tag == 4){
            if(placeView != nil){
                self.arrayHeights.replaceObjectAtIndex(4, withObject: 60)
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                placeView?.hideMenu()
                placeView = nil
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if(textField.tag == 2 || textField.tag == 3){
            let dateFormat = NSDateFormatter()
            dateFormat.timeZone = NSTimeZone.systemTimeZone()
            dateFormat.dateFormat = "HH:mm"
            textField.text = dateFormat.stringFromDate(datePicker.date)
        }
        else if (textField.tag == 4){
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.tableView.contentOffset.y = 250
                }, completion: { (finished) -> Void in
            })
        }
        activeTextField = textField
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag == 4){
            getLocation(textField.text! + string)
        }
        return true
    }
}

//MARK:- TEXTVIEW DELEGATE
extension CreateOfferTableViewController : UITextViewDelegate{
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            textView.resignFirstResponder()
        }else if(textView.text.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) + (text.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) - range.length) > 140){
            return false
        }
        return true
    }
}

//MARK:- DROPDOWN DELEGATE
extension CreateOfferTableViewController : BMDropDownDelegate{
    
    func selectedJobType(jobType: String) {
        arrayTextfields[1].text = jobType
        arrayHeights.replaceObjectAtIndex(1, withObject: 60)
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        buttonDropDown.selected = false
    }
    
    func dropDownHidedSuccessfully() {
        arrayHeights.replaceObjectAtIndex(1, withObject: 60)
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        buttonDropDown.selected = false
    }
}

extension CreateOfferTableViewController : BMAlertDelegate{
    
    func buttonInAlertPressed(sender: UIButton) {
        KGModal.sharedInstance().hideAnimated(true)
    }
    
}

extension CreateOfferTableViewController :GooglePlacesDelegate{
    
    func selectedPlace(placeName: String) {
        placeView = nil
        arrayTextfields[4].text = placeName
        arrayTextfields[4].resignFirstResponder()
        self.arrayHeights.replaceObjectAtIndex(4, withObject: 60)
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        
    }
    
}