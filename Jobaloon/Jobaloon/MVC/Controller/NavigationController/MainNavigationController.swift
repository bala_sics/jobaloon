//
//  MainNavigationController.swift
//  Jobaloon
//
//  Created by Bala on 7/7/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController,UINavigationControllerDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate {
    
    var locationManager : CLLocationManager!
    var viewLine : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.navigationBar.setBackgroundImage(UIImage(named: "NavigationBG")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), forBarMetrics: UIBarMetrics.Default)
        self.navigationBar.translucent = false
        self.interactivePopGestureRecognizer!.delegate = self
        
        ////UPDATE USER LOCATION
        locationManager = locationManager == nil ? CLLocationManager() : locationManager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
         if #available(iOS 8.0, *) {
            locationManager.requestAlwaysAuthorization()
        } else {
            // Fallback on earlier versions
        }
        locationManager.startUpdatingLocation()
        
        viewLine = UIView(frame: CGRectMake(0, -20, 0.5, 66))
        viewLine.backgroundColor = UIColor(patternImage: UIImage(named: "NavigationBG")!)
        self.navigationBar.addSubview(viewLine)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "changeLineColor", name: "SideMenuNotification", object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- NAVIGATION CONTROLLER DELEGATE
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        viewController.navigationItem.hidesBackButton = true
        if(viewController.isKindOfClass(ChatViewController) || viewController.isKindOfClass(MyCandidateTableViewController) || viewController.isKindOfClass(ViewProfileViewController)){
            let leftButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackButton")?.imageWithRenderingMode( UIImageRenderingMode.AlwaysOriginal), style: UIBarButtonItemStyle.Plain, target: self, action: "backButtonPressed")
            viewController.navigationItem.leftBarButtonItem = leftButton
        }else if(!viewController.isKindOfClass(JobTypeTableViewController)){
            setLeftBarButton(viewController)
        }else if ((viewController.valueForKey("fromSideMenu") as! String) == "YES"){
            setLeftBarButton(viewController)
        }
    }
    
    func setLeftBarButton (vc : UIViewController){
        let leftButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "SideMenu")?.imageWithRenderingMode( UIImageRenderingMode.AlwaysOriginal), style: UIBarButtonItemStyle.Plain, target: self, action:
            "sideMenuPressed")
        vc.navigationItem.leftBarButtonItem = leftButton
    }
    
    //MARK:- GESTURE RECOGNISER DELEGATES
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false;
    }
    
    //MARK:- SIDE MENU BUTTON ACTION
    func sideMenuPressed (){
        self.viewLine.backgroundColor = UIColor.whiteColor()
        self.menuContainerViewController.toggleLeftSideMenuCompletion { () -> Void in
      }
    }
    
    //MARK:- BACK BUTTON ACTION
    func backButtonPressed (){
        self.popViewControllerAnimated(true)
    }
    
    //MARK:- CHANGE SEPARATOR LINE COLOR
    func changeLineColor (){
        self.viewLine.backgroundColor = UIColor(patternImage: UIImage(named: "NavigationBG")!)
    }
    
    //MARK:- LOCATION MANAGER DELEGATES
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        USER_LOCATION = (locations as NSArray).firstObject as! CLLocation
        
    }
}
