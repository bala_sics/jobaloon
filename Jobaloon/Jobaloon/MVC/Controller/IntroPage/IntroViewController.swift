//
//  IntroViewController.swift
//  Jobaloon
//
//  Created by Bala on 7/7/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {
    
    @IBOutlet var constraintBottom : NSLayoutConstraint!
    @IBOutlet var constraintCentre : NSLayoutConstraint!
    @IBOutlet var constraintLabel : NSLayoutConstraint!

    
    @IBOutlet var imageViewBackground : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //NAVIGATION BAR HIDDEN
        self.navigationController?.navigationBarHidden = true
        constraintBottom.constant = IS_IPHONE4 ? 5.0 : 63.0
        constraintCentre.constant = IS_IPHONE4 ? 15.0 : 22.0
        constraintLabel.constant = IS_IPHONE6 ? 100 : IS_IPHONE6PLUS  ? 120 : 80
        
        imageViewBackground.image = UIImage(named: IS_IPHONE4 ? "IntroPageBG4" : "IntroPageBG")
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- BUTTON ACTIONS
    //MARK:- CREATE ACCOUNT
    @IBAction func createAccountButtonPressed (sender : UIButton){
        let accountVC : AccountTypeViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AccountTypeVC") as! AccountTypeViewController
        self.navigationController?.pushViewController(accountVC, animated: true)
    }
    
    //MARK:- LOGIN BUTTON
    @IBAction func loginButtonPressed (sender : UIButton){
        let loginVC : LoginTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginTableViewController
        let nav : UINavigationController = UINavigationController(rootViewController: loginVC) as UINavigationController
        nav.navigationBar.translucent = false
        nav.navigationBar.setBackgroundImage(UIImage(named: "NavigationBG")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), forBarMetrics: UIBarMetrics.Default)
        nav.navigationBar.tintColor = UIColor.whiteColor()
        self.presentViewController(nav, animated: true) { () -> Void in
            loginVC.activateEmailField()
        }
    }
    
}
