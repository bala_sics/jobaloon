//
//  MyOffersTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 08/10/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MyOffersTableViewController: UITableViewController {
    
    var arrayMyOffers : NSMutableArray = NSMutableArray()
    var isFirstTime : Bool = true
    var indexCount : NSInteger!
    var _activityIndicatorView : UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Mis candidatos"
        self.tableView.backgroundColor = UIColor(red: 241.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        indexCount = 0;
        getMyOffers()
        setLoadingFooter()
        _activityIndicatorView?.stopAnimating()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- GET MY OFFERS
    func getMyOffers (){
        if(isFirstTime){
            SVProgressHUD.show()
        }
        let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken, "userid" : Person.currentPerson().userId , "index" : "\(indexCount)"]
        Offer.myOffers(Data: params) { (success, result, error) -> Void in
            if(self.indexCount == 0) { self.arrayMyOffers.removeAllObjects() }
            if(success){
                self.arrayMyOffers.addObjectsFromArray(result as! [AnyObject])
                SVProgressHUD.dismiss()
            }else{
                self.isFirstTime ? SVProgressHUD.showErrorWithStatus(error?.localizedDescription) : SVProgressHUD.dismiss()
            }
            self.isFirstTime = false
            self._activityIndicatorView?.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    //MARK:- TABLEVIEW DELEGATES
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayMyOffers.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell")!
        let label : UILabel = cell.viewWithTag(1) as! UILabel
        let offerObj : Offer = arrayMyOffers[indexPath.row] as! Offer
        label.text = offerObj.offerName
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let candidate : MyCandidateTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyCandidateVC") as! MyCandidateTableViewController
        candidate.offerObj = arrayMyOffers[indexPath.row] as! Offer
        self.navigationController?.pushViewController(candidate, animated: true)
    }
    
    //MARK:- SETTING FOOTER LOADING VIEW
    func setLoadingFooter (){
        let frame : CGRect =  CGRectMake(0, 0, self.view.frame.size.width, 30)
        let aframe : CGRect = CGRectMake(self.view.center.x - 10, 20, 5, 5)
        let loadingView : UIView = UIView(frame: frame)
        _activityIndicatorView = UIActivityIndicatorView(frame: aframe)
        _activityIndicatorView?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        _activityIndicatorView?.startAnimating()
        loadingView.addSubview(_activityIndicatorView!)
        self.tableView.tableFooterView = loadingView
    }
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if (self.tableView.contentOffset.y<0)/* scroll from top */
        {
            
        }else if (self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height) && arrayMyOffers.count > 0 ){
            indexCount = indexCount + 10
            self.setLoadingFooter()
            self.getMyOffers()
        }
    }
    
}
