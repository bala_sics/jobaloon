//
//  JobTypeTableViewController.swift
//  Jobaloon
//
//  Created by Bala on 7/8/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class JobTypeTableViewController: UITableViewController,UITextViewDelegate,GooglePlacesDelegate,BMAlertDelegate {
    
    @IBOutlet var textviewLocation : UITextView!
    @IBOutlet var labelDistance : UILabel!
    @IBOutlet var sliderDistance : UISlider!
    @IBOutlet var imageViewBackground : UIImageView!
    @IBOutlet var viewHeader : UIView!

    var arrayJobs : NSMutableArray = NSMutableArray()
    var arrayLocations : NSMutableArray = NSMutableArray()
    var fromSideMenu : String!
    var selectedJob : String = ""
    var searchQuery : SPGooglePlacesAutocompleteQuery!
    var placeView : BMGooglePlaces?
    var sliderValue : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = false
        self.navigationItem.title = "Criterios de búsqueda"
        sliderValue = 10
        //TABLEVIEW BACKGROUND COLOR
        self.tableView.backgroundColor = UIColor(red: 241.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        searchQuery = SPGooglePlacesAutocompleteQuery(apiKey: GOOGLE_APIKEY)
        
        if(fromSideMenu == "NO"){
        let swipe : UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "goBack")
        swipe.direction = UISwipeGestureRecognizerDirection.Right
        self.tableView.addGestureRecognizer(swipe)
        }
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(0.2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue(), { () -> Void in
            self.getJobType()
            self.findCurrentLocation()
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = false
        
    }

    //MARK:- SWIPE GESTURE ACTION
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- DISTANCE SLIDER VALUE CHANGED
    @IBAction func onSliderValueChanged (sender : UISlider){
        sliderValue = Int(sender.value)
        labelDistance.text = "Muéstrame ofertas en un rango de: \(sliderValue)km"
        
    }
    //MARK:- TEXTVIEW DELEGATES
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            textView.resignFirstResponder()
            if(placeView != nil){
                placeView?.hideMenu()
                placeView = nil
            }
        }
        return true
    }
    func textViewDidChange(textView: UITextView) {
        getLocation()
    }
    
    //MARK:- GEOLOCATION
    func getLocation(){
        // searchQuery.location = CLLocationCoordinate2D(latitude: 8.56 , longitude: 76.567)
        searchQuery.input = textviewLocation.text;
        searchQuery.fetchPlaces { (places, error) -> Void in
            if (error != nil) {
                print("error")
                SVProgressHUD.showErrorWithStatus(error.localizedDescription)
            }else{
                self.arrayLocations.removeAllObjects()
                self.arrayLocations.addObjectsFromArray(places  as [AnyObject])
                if(self.placeView == nil){
                    self.placeView = BMGooglePlaces(frame: CGRectMake(CGRectGetMinX(self.imageViewBackground.frame), CGRectGetMaxY(self.imageViewBackground.frame), CGRectGetWidth(self.imageViewBackground.frame), 88))
                    self.placeView?.delegate = self
                    self.placeView!.arrayToLoad = self.arrayLocations.mutableCopy() as! NSMutableArray
                    self.viewHeader.addSubview(self.placeView!)
                }else{
                    self.placeView?.reloadPlaces(self.arrayLocations)
                }
            }
        }
    }
    
    func selectedPlace(placeName: String) {
        placeView = nil
        textviewLocation.text = placeName
        textviewLocation.resignFirstResponder()
    }
    
    //MARK:- GET JOB TYPE
    func getJobType(){
        SVProgressHUD.show()
        Person.getJobTypes { (success, result, error) -> Void in
            if(success){
                let array : NSArray = result as! NSArray
                let sortedArray : NSArray = array.sortedArrayUsingSelector("localizedCaseInsensitiveCompare:")
                self.arrayJobs.addObjectsFromArray(sortedArray as [AnyObject])
                SVProgressHUD.dismiss()
                self.tableView.reloadData()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    //MARK:- FIND CURRENT LOCATION
    func findCurrentLocation(){
        let geocode : GeoCoder = GeoCoder()
        geocode.reverseGeoCode(USER_LOCATION, inBlock: { (result) -> Void in
            if(result == nil){
                self.textviewLocation.text = ""
            }else{
                let addressDict : NSDictionary = result as NSDictionary
                if(addressDict["address"] != nil){
                    self.textviewLocation.text = addressDict["address"] as! String
                }
            }
        })
    }
    
    // MARK: - TABLEVIEW DELEGATES
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayJobs.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 75
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) 
        
        let label : UILabel = cell.viewWithTag(100) as! UILabel
        label.text = arrayJobs[indexPath.row] as? String
        label.textColor = selectedJob == arrayJobs[indexPath.row] as! String ? UIColor.whiteColor() : UIColor.blackColor()
        
        let viewBG  = cell.viewWithTag(101)
        viewBG!.backgroundColor = selectedJob == arrayJobs[indexPath.row] as! String ? UIColor(patternImage: UIImage(named: "NavigationBG")!) : UIColor.whiteColor()
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedJob = arrayJobs[indexPath.row] as! String
        if(textviewLocation.text == ""){
            showAlert("Por favor, seleccione cualquier ubicación")
        }else{
            USERDICT.setValue(textviewLocation.text, forKey: "address")
            USERDICT.setValue(selectedJob, forKey: "jobtype")
            USERDICT.setValue("\(sliderValue)", forKey: "distance")
            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nav : MainNavigationController = storyboard.instantiateViewControllerWithIdentifier("RootNav") as! MainNavigationController
            nav.viewControllers[0].setValue(textviewLocation.text, forKey: "address")
            nav.viewControllers[0].setValue(selectedJob, forKey: "jobtype")
            let sideNavigation : SideMenuTableViewController = storyboard.instantiateViewControllerWithIdentifier("SideMenuVC") as! SideMenuTableViewController
            AppDelegate.getAppdelegateInstance().containerSideMenu = MFSideMenuContainerViewController.containerWithCenterViewController(nav, leftMenuViewController: sideNavigation, rightMenuViewController: nil)
            AppDelegate.getAppdelegateInstance().containerSideMenu.menuWidth = CGFloat(screenSize.width - 70)
            AppDelegate.getAppdelegateInstance().window?.rootViewController = AppDelegate.getAppdelegateInstance().containerSideMenu
        }
        self.tableView.reloadData()
    }
    
    //MARK:- SHOW ALERT
    func showAlert (text : String){
        let alertView : BMAlert = (NSBundle.mainBundle().loadNibNamed("BMAlert", owner: self, options: nil) as NSArray)[0] as! BMAlert
        alertView.delegate = self
        alertView.configureView(title: "Error", message: text, buttonTitle: "Ok", labelAlignment: NSTextAlignment.Center)
        KGModal.sharedInstance().showWithContentView(alertView)
    }
    
    func buttonInAlertPressed(sender: UIButton) {
        KGModal.sharedInstance().hideAnimated(true)
    }
    
}
