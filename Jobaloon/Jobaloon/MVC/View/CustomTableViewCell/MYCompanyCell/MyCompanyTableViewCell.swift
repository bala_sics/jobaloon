//
//  MyCompanyTableViewCell.swift
//  Jobaloon
//
//  Created by Bala on 9/28/15.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MyCompanyTableViewCell: UITableViewCell {

    var imageViewProfile : UIImageView!
    var labelLastChat : UILabel!
    var labelCompanyName : UILabel!
    var badgeNotification : JSCustomBadge!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        ///PROFILE IMAGEVIEW
        imageViewProfile = UIImageView(frame: CGRectMake(10, 10, 55, 55))
        imageViewProfile.contentMode = UIViewContentMode.ScaleToFill
        imageViewProfile.layer.cornerRadius = imageViewProfile.frame.width / 2.0
        imageViewProfile.clipsToBounds = true
        imageViewProfile.image = UIImage(named: "ImagePlaceholder")
        self.contentView.addSubview(imageViewProfile)
        
//        let path : UIBezierPath = UIBezierPath(ovalInRect: imageViewProfile.bounds)
//        let maskLayer : CAShapeLayer = CAShapeLayer()
//        maskLayer.path = path.CGPath
//        imageViewProfile.layer.mask = maskLayer;
        
        ///LABEL COMPANY NAME
        labelCompanyName = UILabel(frame: CGRectMake(75,  10, screenSize.width - 80, 25))
        labelCompanyName.font = UIFont.boldSystemFontOfSize(15)
        labelCompanyName.textColor = UIColor.blackColor()
        self.contentView.addSubview(labelCompanyName)
        
        ///LABEL LAST CHAT
        labelLastChat = UILabel(frame: CGRectMake(75, 40, screenSize.width - 80, 25))
        labelLastChat.font = UIFont.systemFontOfSize(13)
        labelLastChat.textColor = UIColor.lightGrayColor()
        self.contentView.addSubview(labelLastChat)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell (appObj : MyApplication.CompanyDetails){
        
        imageViewProfile.sd_setImageWithURL(NSURL(string: imagePath + appObj.userImage), placeholderImage: UIImage(named: "ImagePlaceholder"))
        labelLastChat.text = appObj.userLastMessage
        labelCompanyName.text = appObj.username
        setCustomBadgeWithCount(count: Int(appObj.userMessageUnreadCount)!)
    }

    //MARK:- CUSTOM BADGE
    func setCustomBadgeWithCount (count count : NSInteger){
        
        if (count == 0) {
            if (badgeNotification != nil) {  badgeNotification.removeFromSuperview() }
        }else
        {
            if (badgeNotification != nil) {  badgeNotification.removeFromSuperview() }
            badgeNotification = JSCustomBadge(string: "\(count)")
            badgeNotification.frame = CGRectMake(screenSize.width - 50, 25 , 24, 24)
            self.contentView.addSubview(badgeNotification)
        }
    }
}
