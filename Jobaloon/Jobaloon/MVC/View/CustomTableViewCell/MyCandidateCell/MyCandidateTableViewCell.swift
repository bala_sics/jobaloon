//
//  MyCandidateTableViewCell.swift
//  Jobaloon
//
//  Created by Bala on 09/10/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MyCandidateTableViewCell: UITableViewCell {

    var imageViewProfile : UIImageView!
    var labelUserName : UILabel!
    var buttonSeeProfile : UIButton!
    var buttonChat : UIButton!
    var badgeNotification : JSCustomBadge!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        ///BACKGROUND VIEW
        let bgView : UIView = UIView(frame: CGRectMake(10, 5, screenSize.width - 20, 60))
        bgView.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(bgView)
        
        ///PROFILE IMAGEVIEW
        imageViewProfile = UIImageView(frame: CGRectMake(0, 0, 60, 60))
        imageViewProfile.contentMode = UIViewContentMode.ScaleToFill
        imageViewProfile.clipsToBounds = true
        imageViewProfile.image = UIImage(named: "ImagePlaceholder")
        bgView.addSubview(imageViewProfile)
        
        ///LABEL USER NAME
        labelUserName = UILabel(frame: CGRectMake(65,  10, screenSize.width - 90, 25))
        labelUserName.font = UIFont.boldSystemFontOfSize(13)
        labelUserName.textColor = UIColor.grayColor()
        bgView.addSubview(labelUserName)
        
        //BUTTON SEE PROFILE
        buttonSeeProfile = UIButton(type: UIButtonType.Custom)
        buttonSeeProfile.frame = CGRectMake(65, 32, 70, 25)
        buttonSeeProfile.setTitleColor(UIColor(red: 0/255.0, green: 169.0/255.0, blue: 211.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        buttonSeeProfile.setTitle("See profile", forState: UIControlState.Normal)
        buttonSeeProfile.titleLabel?.font = UIFont.systemFontOfSize(13)
        bgView.addSubview(buttonSeeProfile)
        
        /////BUTTON CHAT
        buttonChat = UIButton(type: UIButtonType.Custom)
        buttonChat.frame = CGRectMake(screenSize.width - 100, 32, 70, 25)
        buttonChat.setTitleColor(UIColor(red: 0/255.0, green: 169.0/255.0, blue: 211.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        buttonChat.setTitle("Chat", forState: UIControlState.Normal)
        buttonChat.titleLabel?.font = UIFont.systemFontOfSize(13)
        bgView.addSubview(buttonChat)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureCell (candidateObj : MyCandidate){
        /////////CUSTOMIZING USERNAME AND OFFER STATUS
        buttonChat.hidden = !(candidateObj.candidateOfferStatus == "Preselected" || candidateObj.candidateCompanyStatus == "Preselected")
        var color : UIColor!
        if (candidateObj.candidateOfferStatus == "Preselected"){
            color = UIColor(red: 0/255.0, green: 169.0/255.0, blue: 211.0/255.0, alpha: 1.0)
        }else if (candidateObj.candidateOfferStatus == "Discarded"){
            color = UIColor(red: 234/255.0, green: 108.0/255.0, blue: 113.0/255.0, alpha: 1.0)
        }else{
            color = UIColor(red: 244.0/255.0, green: 162.0/255.0, blue: 0/255.0, alpha: 1.0)
        }
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: "\(candidateObj.candidateUsername)-     \(candidateObj.candidateOfferStatus)")
        attString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(candidateObj.candidateUsername.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) + 6, candidateObj.candidateOfferStatus.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)))
        attString.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(12), range: NSMakeRange(candidateObj.candidateUsername.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) + 6, candidateObj.candidateOfferStatus.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)))
        
        labelUserName.attributedText = attString
        imageViewProfile.image = UIImage(named: "ImagePlaceholder")
        if(candidateObj.candidateOfferStatus == "Preselected"){
            setCustomBadgeWithCount(count:Int(candidateObj.candidateUnreadCount)!)
        }else{
             if (badgeNotification != nil) {  badgeNotification.removeFromSuperview() }
        }
    }
    
    //MARK:- CUSTOM BADGE
    func setCustomBadgeWithCount (count count : NSInteger){
        
        if (count == 0) {
            if (badgeNotification != nil) {  badgeNotification.removeFromSuperview() }
        }else
        {
            if (badgeNotification != nil) {  badgeNotification.removeFromSuperview() }
            badgeNotification = JSCustomBadge(string: "\(count)")
            badgeNotification.frame = CGRectMake(CGRectGetMinX(buttonChat.frame) + 5, 37 , 24, 24)
            self.contentView.addSubview(badgeNotification)
        }
    }
}
