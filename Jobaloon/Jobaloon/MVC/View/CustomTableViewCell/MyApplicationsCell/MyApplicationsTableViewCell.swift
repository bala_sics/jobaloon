//
//  MyApplicationsTableViewCell.swift
//  Jobaloon
//
//  Created by Bala on 9/25/15.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MyApplicationsTableViewCell: UITableViewCell {

    var imageViewProfile : UIImageView!
    var labelOfferRole : UILabel!
    var labelCompanyName : UILabel!
      var labelStatus : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        ///PROFILE IMAGEVIEW
        imageViewProfile = UIImageView(frame: CGRectMake(10, 15, 70, 70))
        imageViewProfile.contentMode = UIViewContentMode.ScaleToFill
        imageViewProfile.layer.cornerRadius = 5.0
        imageViewProfile.clipsToBounds = true
        imageViewProfile.image = UIImage(named: "ImagePlaceholder")
        self.contentView.addSubview(imageViewProfile)
        
        ///LABEL OFFER ROLE
        labelOfferRole = UILabel(frame: CGRectMake(95,  18, screenSize.width - 100, 25))
        labelOfferRole.font = UIFont.boldSystemFontOfSize(13)
        labelOfferRole.textColor = UIColor.blackColor()
        self.contentView.addSubview(labelOfferRole)
        
        ///LABEL COMPANY NAME
        labelCompanyName = UILabel(frame: CGRectMake(95, 38, screenSize.width - 100, 25))
        labelCompanyName.font = UIFont.systemFontOfSize(13)
        labelCompanyName.textColor = UIColor.lightGrayColor()
        self.contentView.addSubview(labelCompanyName)
        
        ///LABEL STATUS
        labelStatus = UILabel(frame: CGRectMake(95, 58, screenSize.width - 100, 25))
        labelStatus.font = UIFont.boldSystemFontOfSize(11)
        labelStatus.textColor = UIColor.lightGrayColor()
        self.contentView.addSubview(labelStatus)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func configureCell (appObj : MyApplication){
        
        imageViewProfile.sd_setImageWithURL(NSURL(string: imagePath + appObj.offerImage), placeholderImage: UIImage(named: "ImagePlaceholder"))
        labelOfferRole.text = appObj.offerRole
        labelCompanyName.text = appObj.offerCompanyName
        labelStatus.text = "Application \(appObj.offerStatus)"
        labelStatus.textColor = setStatusColor(appObj.offerStatus)
    }

    
    func setStatusColor (status : String) -> UIColor{
        var color : UIColor!
        switch status{
        case "Accepted" :
            color = UIColor(red: 95.0/255.0, green: 201.0/255.0, blue: 235.0/255.0, alpha: 1.0)
            break
        case "Rejected" :
             color = UIColor(red: 191.0/255.0, green: 3.0/255.0, blue: 33.0/255.0, alpha: 1.0)
            break
        case "Pending" :
             color = UIColor(red: 247.0/255.0, green: 186.0/255.0, blue: 103.0/255.0, alpha: 1.0)
            break
        default :
           color = UIColor.lightGrayColor()
            break
        }
           return color
        }
    }

