//
//  OfferView.swift
//  Jobaloon
//
//  Created by Bala on 7/15/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation

class OfferView: UIView {
    
    @IBOutlet var imageViewOffer : UIImageView!
    var imageViewPlay : UIImageView!
    var labelJobHeading : UILabel!
    var labelJobDescription : UILabel!
    var labelJobLocation : UILabel!
    var labelJobDate : UILabel!
    var labelJobDays : UILabel!
    var offerCopy : Offer!
    var jobseekerCopy : Offer.JobSeekers!
    var moviePlayer : MPMoviePlayerController!
    var movieUrl : NSURL!
    var activityIndicator : UIActivityIndicatorView!
    var labelLastOffer1 : UILabel!
    var labelLastOffer2 : UILabel!
    var labellastOffer3 : UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        self.clipsToBounds = true
        let imageviewBG : UIImageView = UIImageView(frame: self.bounds)
        imageviewBG.image = UIImage(named: "OfferBackground")
        self.addSubview(imageviewBG)
        
        imageViewOffer =  IS_IPHONE4 ? UIImageView(frame: CGRectMake(0, 0, self.frame.width , 180))   : UIImageView(frame: CGRectMake(0, 0, self.frame.width , self.frame.width))
        imageViewOffer.image = UIImage(named: "SampleOfferImage")
        imageViewOffer.userInteractionEnabled = true
        self.addSubview(imageViewOffer)
        
        //LABEL JOB HEADING
        labelJobHeading = UILabel(frame: CGRectMake(10, CGRectGetMaxY(imageViewOffer.frame), self.frame.width - 10, 35))
        labelJobHeading.font = UIFont(name: Helvetica, size: 13.0)
        self.addSubview(labelJobHeading)
        
        //LABEL JOB DESCRIPTION
        labelJobDescription = UILabel(frame: CGRectMake(10, CGRectGetMaxY(labelJobHeading.frame) - 10, self.frame.width - 10, 40))
        labelJobDescription.font = UIFont(name: Helvetica, size: 13.0)
        labelJobDescription.textColor = UIColor.lightGrayColor()
        labelJobDescription.numberOfLines = 2
        labelJobDescription.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.addSubview(labelJobDescription)
        
        //LABEL JOB LOCATION
        labelJobLocation = UILabel(frame: CGRectMake(10, CGRectGetMaxY(labelJobDescription.frame) - 10, self.frame.width - 10, 25))
        labelJobLocation.font = UIFont(name: Helvetica, size: 13.0)
        labelJobLocation.textColor = UIColor.lightGrayColor()
        self.addSubview(labelJobLocation)
        
        //LABEL JOB DATE
        labelJobDate = UILabel(frame: CGRectMake(10, CGRectGetMaxY(labelJobLocation.frame) - 10, self.frame.width - 10, 25))
        labelJobDate.font = UIFont(name: Helvetica, size: 13.0)
        labelJobDate.textColor = UIColor.lightGrayColor()
        self.addSubview(labelJobDate)
        
        //LABEL JOB DAYS
        labelJobDays = UILabel(frame: CGRectMake(10, CGRectGetMaxY(labelJobDate.frame) - 10, self.frame.width - 10, 25))
        labelJobDays.font = UIFont(name: Helvetica, size: 13.0)
        labelJobDays.textColor = UIColor.lightGrayColor()
        self.addSubview(labelJobDays)
        
        labelLastOffer1 = UILabel(frame: CGRectMake(10, CGRectGetMaxY(labelJobDays.frame) - 10, self.frame.width - 10, 40))
        labelLastOffer1.font = UIFont(name: Helvetica, size: 13.0)
        labelLastOffer1.textColor = UIColor.lightGrayColor()
        labelLastOffer1.numberOfLines = 2
        labelLastOffer1.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.addSubview(labelLastOffer1)

        labelLastOffer2 = UILabel(frame: CGRectMake(10, CGRectGetMaxY(labelLastOffer1.frame) - 10, self.frame.width - 10, 40))
        labelLastOffer2.font = UIFont(name: Helvetica, size: 13.0)
        labelLastOffer2.textColor = UIColor.lightGrayColor()
        labelLastOffer2.numberOfLines = 2
        labelLastOffer2.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.addSubview(labelLastOffer2)

        labellastOffer3 = UILabel(frame: CGRectMake(10, CGRectGetMaxY(labelLastOffer2.frame) - 10, self.frame.width - 10, 40))
        labellastOffer3.font = UIFont(name: Helvetica, size: 13.0)
        labellastOffer3.textColor = UIColor.lightGrayColor()
        labellastOffer3.numberOfLines = 2
        labellastOffer3.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.addSubview(labellastOffer3)

        
        //PLAY BUTTON
        imageViewPlay = UIImageView(frame: CGRectMake(imageViewOffer.frame.width/2 - 30, imageViewOffer.frame.height/2 - 30, 60, 60))
        imageViewPlay.image = UIImage(named: "Play")
        self.addSubview(imageViewPlay)
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(imageViewOffer.frame.width/2 - 20, imageViewOffer.frame.height/2 - 20, 40, 40))
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        activityIndicator.hidesWhenStopped = true
        self.addSubview(activityIndicator)
    }
    
    func configureView (offerObj : Offer){
        offerCopy = offerObj
        labelJobHeading.text = offerObj.offerRole
        labelJobDescription.text = offerObj.offerJobDescription
        labelJobLocation.text = offerObj.offerLocation
        labelLastOffer1.text = offerObj.offerDescription1
        labelLastOffer2.text = offerObj.offerDescription2
        labellastOffer3.text = offerObj.offerDescription3
        labelJobDate.text = offerObj.offerStartDate == "" || offerObj.offerEndDate == "" ? "" : "\(offerObj.offerStartDate) - \(offerObj.offerEndDate) h"
        labelJobDays.text = offerObj.offerWorkingDays
        imageViewOffer.sd_setImageWithURL(NSURL(string: imagePath + offerObj.offerImage), placeholderImage: UIImage(named: "ImagePlaceholder"))
        imageViewPlay.hidden = true
    }
    
    func configureJobSeekerView (personObj : Offer.JobSeekers){
        jobseekerCopy = personObj
        labelJobHeading.text = personObj.username
        labelJobDescription.text = personObj.userEmail
        labelJobLocation.text = personObj.userAddress
        labelJobDate.text = ""
        labelJobDays.text = ""
        imageViewOffer.sd_setImageWithURL(NSURL(string: imagePath + personObj.userImage), placeholderImage: UIImage(named: "VideoPlaceholder"))
       // movieUrl = NSURL(string: "http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4")

        movieUrl = NSURL(string: videoPath + personObj.userVideo)
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onThumbnailImageTapped")
        tapGesture.numberOfTapsRequired = 1
        imageViewOffer.addGestureRecognizer(tapGesture)
        
        
        if(movieUrl == nil){ return }
        imageViewPlay.hidden = false
        moviePlayer = MPMoviePlayerController(contentURL: movieUrl)
        moviePlayer.scalingMode = MPMovieScalingMode.AspectFill
        moviePlayer.view.frame = imageViewOffer.frame
        moviePlayer.movieSourceType = MPMovieSourceType.File
        moviePlayer.controlStyle = MPMovieControlStyle.None
        //        moviePlayer.prepareToPlay()

    }
    
    func configureNoOfferPage (){
        imageViewPlay.hidden = true
        imageViewOffer.image = UIImage(named: "OfferPlaceholder")
        labelJobHeading.text = "No hay más ofertas de este tipo. Ve a ajustes para cambiar tus preferencias de búsqueda y encontrar nuevas ofertas"
        labelJobHeading.numberOfLines = 3
        labelJobHeading.font = UIFont(name: Helvetica, size: 12.0)
        labelJobHeading.frame = CGRectMake(labelJobHeading.frame.origin.x, labelJobHeading.frame.origin.y, labelJobHeading.frame.width, 60)
        labelJobHeading.textColor = UIColor.redColor()
        labelJobLocation.text = "Equipo de Jobaloon"
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func configureProfileView (cand :MyCandidate){
        labelJobHeading.text = cand.candidateUsername
        labelJobDescription.text = cand.candidateUserEmail
        labelJobLocation.text = cand.candidateUserAddress
        labelJobDate.text = ""
        labelJobDays.text = ""
        imageViewOffer.sd_setImageWithURL(NSURL(string: imagePath + cand.candidateUserImage), placeholderImage: UIImage(named: "VideoPlaceholder"))
        movieUrl = NSURL(string: videoPath + cand.candidateUserVideo)
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onThumbnailImageTapped")
        tapGesture.numberOfTapsRequired = 1
        imageViewOffer.addGestureRecognizer(tapGesture)
        
        if(movieUrl == nil){ return }
        imageViewPlay.hidden = false
        moviePlayer = MPMoviePlayerController(contentURL: movieUrl)
        moviePlayer.scalingMode = MPMovieScalingMode.AspectFill
        moviePlayer.view.frame = imageViewOffer.frame
        moviePlayer.movieSourceType = MPMovieSourceType.File
//        moviePlayer.prepareToPlay()
        moviePlayer.controlStyle = MPMovieControlStyle.None
    }

    func startPlayingVideo (){
        imageViewPlay.hidden = true
        if(moviePlayer == nil){
        moviePlayer = MPMoviePlayerController(contentURL: movieUrl)
        moviePlayer.scalingMode = MPMovieScalingMode.AspectFill
        moviePlayer.view.frame = imageViewOffer.frame
        moviePlayer.movieSourceType = MPMovieSourceType.File
//        moviePlayer.prepareToPlay()
        moviePlayer.controlStyle = MPMovieControlStyle.None
        }
        moviePlayer.initialPlaybackTime = -2.0;
        moviePlayer.play()
//        imageViewOffer.superview!.insertSubview(moviePlayer.view, belowSubview: imageViewPlay)
        activityIndicator.startAnimating()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "playBackCompleted:", name: MPMoviePlayerPlaybackDidFinishNotification, object: moviePlayer)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "movieplayerStateChanged:", name: MPMoviePlayerLoadStateDidChangeNotification, object: moviePlayer)

        moviePlayer.view.userInteractionEnabled = true
        
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onMoviePlayerTapped")
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate = self
        moviePlayer.view.addGestureRecognizer(tapGesture)
        
        moviePlayer.backgroundView.backgroundColor = UIColor.clearColor()
    

    }
    
    func movieplayerStateChanged (aNotification : NSNotification){
        let player : MPMoviePlayerController = aNotification.object as! MPMoviePlayerController
        if( player.loadState ==  MPMovieLoadState.Playable ||  player.loadState ==  MPMovieLoadState.PlaythroughOK)
        {
            self.performSelector("insertPlayerView", withObject: nil, afterDelay: 1.5)

        }
    }

    func insertPlayerView (){
        activityIndicator.stopAnimating()
        self.insertSubview(moviePlayer.view, belowSubview: imageViewPlay)

    }
    func stopPlayingVideo (){
        if(moviePlayer != nil){
            moviePlayer.stop()
            moviePlayer.view.removeFromSuperview()
            moviePlayer = nil
            imageViewPlay.hidden = false
            activityIndicator.stopAnimating()
        }
    }
    
    //MARK:- MPMOVIE PLAYER
    func onThumbnailImageTapped(){
        startPlayingVideo()
    }
    
//    override func becomeFirstResponder() -> Bool {
//        return true
//    }
    func onMoviePlayerTapped(){
       if(moviePlayer.playbackState.rawValue == 1){
            moviePlayer.pause()
        imageViewPlay.hidden = false

        }else{
        moviePlayer.play()
        imageViewPlay.hidden = true
        }
    }
    
    //MARK:- DID FINISH PLAYING VIDEO NOTIFICATION
    func playBackCompleted (aNotification : NSNotification){
        let player : MPMoviePlayerController = aNotification.object as! MPMoviePlayerController
        player.stop()
        player.view.removeFromSuperview()
        imageViewPlay.hidden = false
        activityIndicator.stopAnimating()
    }
}

//MARK:- GESTURE RECOGNISER DELEGATES
extension OfferView : UIGestureRecognizerDelegate{
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

}