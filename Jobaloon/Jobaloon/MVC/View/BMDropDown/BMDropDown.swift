//
//  BMDropDown.swift
//  Jobaloon
//
//  Created by Bala on 10/5/15.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

protocol BMDropDownDelegate{
    func selectedJobType(jobType : String)
    func dropDownHidedSuccessfully ()
}

class BMDropDown: UIView ,UITableViewDelegate,UITableViewDataSource{
    var tableViewObj : UITableView!
    var arrayToLoad : NSMutableArray = NSMutableArray()
    var delegate : BMDropDownDelegate!


    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        tableViewObj = UITableView(frame:  CGRectMake(0, 0, self.bounds.width, 0), style: UITableViewStyle.Plain)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        tableViewObj.backgroundColor = UIColor.whiteColor()
        tableViewObj.showsVerticalScrollIndicator = false
        tableViewObj.layer.borderColor = UIColor.lightGrayColor().CGColor
        tableViewObj.layer.borderWidth = 0.5
        tableViewObj.layer.cornerRadius = 3.0
        self .addSubview(tableViewObj)
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 176)
            }) { (finished) -> Void in
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func hideMenu(){
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
            }) { (finished) -> Void in
                self.removeFromSuperview()
                self.delegate.dropDownHidedSuccessfully()
        }
    }
    //MARK:- TABLEVIEW DELEGATES
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayToLoad.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        // iOS 7
        if(tableView.respondsToSelector(Selector("setSeparatorInset:"))){
            tableView.separatorInset = UIEdgeInsetsZero
        }
        // iOS 8
        if(tableView.respondsToSelector(Selector("setLayoutMargins:"))){
            if #available(iOS 8.0, *) {
                tableView.layoutMargins = UIEdgeInsetsZero
            } else {
                // Fallback on earlier versions
            };
        }
        if(cell.respondsToSelector(Selector("setLayoutMargins:"))){
            if #available(iOS 8.0, *) {
                cell.layoutMargins = UIEdgeInsetsZero
            } else {
                // Fallback on earlier versions
            };
            
            
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell  = tableView.dequeueReusableCellWithIdentifier("cel")
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None;
            cell?.backgroundColor = UIColor.clearColor()
            cell?.contentView.backgroundColor = UIColor.clearColor()
        }
      
        cell?.textLabel?.text = arrayToLoad[indexPath.row] as? String
        cell?.textLabel?.font = UIFont.systemFontOfSize(13)
        cell?.textLabel?.textColor = UIColor.lightGrayColor()
        cell?.textLabel?.textAlignment = NSTextAlignment.Left
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell : UITableViewCell = tableViewObj.cellForRowAtIndexPath(indexPath)!
        cell.contentView.backgroundColor = UIColor(patternImage: UIImage(named: "NavigationBG")!)
        cell.textLabel?.textColor = UIColor.whiteColor()
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
            }) { (finished) -> Void in
                
                self.delegate.selectedJobType((self.arrayToLoad[indexPath.row] as! String))
                self.removeFromSuperview()
        }
    }

}
