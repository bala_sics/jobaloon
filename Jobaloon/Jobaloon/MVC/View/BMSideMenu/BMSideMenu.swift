//
//  BMSideMenu.swift
//  Jobaloon
//
//  Created by Bala on 7/7/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class BMSideMenu: UIView,UITableViewDelegate,UITableViewDataSource {
    
    var tableViewObj : UITableView!
    var arrayToLoad : NSMutableArray = NSMutableArray()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SHOW SIDE MENU
    func showSidemenu(Frame Frame : CGRect ){
        self.frame = UIScreen.mainScreen().bounds
        self.backgroundColor = UIColor.clearColor()
        tableViewObj = UITableView(frame:  Frame, style: UITableViewStyle.Plain)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        tableViewObj.backgroundColor = UIColor(red: 18.0/255.0, green: 158.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        tableViewObj.showsVerticalScrollIndicator = false
        self .addSubview(tableViewObj)
        let appde : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appde.window!.addSubview(self)
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame) - 100, 64, CGRectGetWidth(self.tableViewObj.frame), Frame.size.height)
            }) { (finished) -> Void in
        }
    }
    
    //MARK:- TABLEVIEW DELEGATES
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayToLoad.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cel")
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None;
            cell?.backgroundColor = UIColor(red: 18.0/255.0, green: 158.0/255.0, blue: 204.0/255.0, alpha: 1.0)
            cell?.contentView.backgroundColor = UIColor(red: 18.0/255.0, green: 158.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        }
        cell?.textLabel?.text = arrayToLoad[indexPath.row] as? String
        cell?.textLabel?.font = UIFont.systemFontOfSize(15)
        cell?.textLabel?.textColor = UIColor.whiteColor()
        cell?.textLabel?.textAlignment = NSTextAlignment.Center
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Selected Value \(arrayToLoad[indexPath.row] as? String)")
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame) + 100, 64, CGRectGetWidth(self.tableViewObj.frame), 220)
            }) { (finished) -> Void in
                self.removeFromSuperview()
        }
    }
    
    //MARK:- UIVIEW DELEGATES
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = (touches as Set).first
        let location = touch!.locationInView(touch!.view)
        if(!CGRectContainsPoint(self.tableViewObj.frame, location)){
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame) + 100, 64, CGRectGetWidth(self.tableViewObj.frame), 220)
                }) { (finished) -> Void in
                    self.removeFromSuperview()
            }
        }
    }
    
}
