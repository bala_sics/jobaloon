//
//  BMAlert.swift
//  Jobaloon
//
//  Created by Bala on 7/9/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

protocol BMAlertDelegate {
    func buttonInAlertPressed(sender : UIButton)
}
class BMAlert: UIView {
    
    @IBOutlet var labelTitle : UILabel!
    @IBOutlet var labelMessage : UILabel!
    @IBOutlet var button : UIButton!
    
    var delegate : BMAlertDelegate!
    
    //MARK:- CONFIGURE VIEW
    func configureView (title title : String , message : String , buttonTitle : String , labelAlignment : NSTextAlignment){
        labelTitle.text = title
        labelMessage.text =  message
        labelMessage.textAlignment = labelAlignment
        let height : CGFloat = labelMessage.heightForLabel(message, font: UIFont.systemFontOfSize(13), width: 261).height
        self.frame = CGRectMake(0, 0, 281, height < 39 ? 171 :  (134 + height))
        button.setTitle(buttonTitle, forState: UIControlState.Normal)
    }
    
    //MARK:- BUTTON ACTION
    @IBAction func ButtonPressed(sender : UIButton){
        delegate.buttonInAlertPressed(sender)
    }
    
    
}
