//
//  GooglePlaces.swift
//  Jobaloon
//
//  Created by Bala on 7/9/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

protocol GooglePlacesDelegate{
    func selectedPlace(placeName : String)
}

class BMGooglePlaces: UIView,UITableViewDelegate,UITableViewDataSource
{
    var tableViewObj : UITableView!
    var arrayToLoad : NSMutableArray = NSMutableArray()
    var delegate : GooglePlacesDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        tableViewObj = UITableView(frame:  CGRectMake(0, 0, self.bounds.width, 0), style: UITableViewStyle.Plain)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        tableViewObj.backgroundColor = UIColor.whiteColor()
        tableViewObj.showsVerticalScrollIndicator = false
        tableViewObj.layer.borderColor = UIColor.blackColor().CGColor
        tableViewObj.layer.borderWidth = 1.0
        tableViewObj.layer.cornerRadius = 3.0
        self .addSubview(tableViewObj)
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 88)
            }) { (finished) -> Void in
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- RELOAD PAGE
    func reloadPlaces(arrayPlaces : NSArray){
        self.arrayToLoad.removeAllObjects()
        self.arrayToLoad.addObjectsFromArray(arrayPlaces as [AnyObject])
        self.tableViewObj.reloadData()
    }
    
    func hideMenu(){
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
            }) { (finished) -> Void in
                self.removeFromSuperview()
        }
    }
    //MARK:- TABLEVIEW DELEGATES
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayToLoad.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell  = tableView.dequeueReusableCellWithIdentifier("cel")
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None;
            cell?.backgroundColor = UIColor.clearColor()
            cell?.contentView.backgroundColor = UIColor.clearColor()
        }
        let place : SPGooglePlacesAutocompletePlace = self.arrayToLoad[indexPath.row] as! SPGooglePlacesAutocompletePlace
        
        cell?.textLabel?.text = place.name
        cell?.textLabel?.font = UIFont.systemFontOfSize(13)
        cell?.textLabel?.textColor = UIColor.blackColor()
        cell?.textLabel?.textAlignment = NSTextAlignment.Center
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
            }) { (finished) -> Void in
                let place : SPGooglePlacesAutocompletePlace = self.arrayToLoad[indexPath.row] as! SPGooglePlacesAutocompletePlace
                
                self.delegate.selectedPlace(place.name)
                self.removeFromSuperview()
        }
    }
}
