//
//  MyCandidate.swift
//  Jobaloon
//
//  Created by Bala on 09/10/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MyCandidate: NSObject {

    var candidateUsername : String!
    var candidateUserId : String!
    var candidateUserEmail : String!
    var candidateUserPassword : String!
    var candidateUserLabels : String!
    var candidateUserCode : String!
    var candidateUserAddress : String!
    var candidateUserDateOfJoin : String!
    var candidateUserLastActivity : String!
    var candidateUserLastActivityDate : String!
    var candidateUserType : String!
    var candidateUserLatitude : String!
    var candidateUserLongitude : String!
    var candidateUserVideo : String!
    var candidateUserImage : String!
    var candidateUserNotes : String!
    var candidateUserStatus : String!
    var candidateUserJobId : String!
    var candidateUserJobDescription : String!
    var candidateUserExperience1 : String!
    var candidateUserExperience2 : String!
    var candidateUserExperience3 : String!
    var candidateUserDistance : String!
    var candidateOfferStatus : String!
    var candidateCompanyStatus : String!
    var candidateUnreadCount : String!
    
    init(dict : NSDictionary) {
        super.init()
        self.candidateUsername =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_name") as? String)
        let userid = dict["user_id"] as! NSNumber
        self.candidateUserId =  "\(userid)"
        self.candidateUserEmail =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_email") as? String)
        self.candidateUserPassword = CheckForNull(strToCheck: dict.checkValueForKey(key: "password") as? String)
        self.candidateUserAddress =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_address") as? String)
        self.candidateUserLabels =  CheckForNull(strToCheck: dict.checkValueForKey(key: "job_type") as? String)
        self.candidateUserCode = CheckForNull(strToCheck: dict.checkValueForKey(key: "code") as? String)
        self.candidateUserType =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_type") as? String)
        self.candidateUserDateOfJoin =  CheckForNull(strToCheck: dict.checkValueForKey(key: "dateOfJoin") as? String)
        self.candidateUserLastActivity =  CheckForNull(strToCheck: dict.checkValueForKey(key: "Last_activity") as? String)
        self.candidateUserLastActivityDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "Last_activity_date") as? String)
        self.candidateUserLatitude =  CheckForNull(strToCheck: dict.checkValueForKey(key: "lattitude") as? String)
        self.candidateUserLongitude =  CheckForNull(strToCheck: dict.checkValueForKey(key: "longitude")as? String)
        self.candidateUserVideo = CheckForNull(strToCheck: dict.checkValueForKey(key: "video") as? String)
        self.candidateUserImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
        self.candidateUserNotes =  CheckForNull(strToCheck: dict.checkValueForKey(key: "Notes") as? String)
        self.candidateUserStatus = CheckForNull(strToCheck: dict.checkValueForKey(key: "user_status") as? String)
        self.candidateUserJobId = CheckForNull(strToCheck: dict.checkValueForKey(key: "JobId") as? String)
        self.candidateUserJobDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "Jobdescription") as? String)
        self.candidateUserExperience1 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience1") as? String)
        self.candidateUserExperience2 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience2") as? String )
        self.candidateUserExperience3 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience3") as? String)
        self.candidateUserDistance = CheckForNull(strToCheck: dict.checkValueForKey(key: "distance") as? String)
        self.candidateOfferStatus = CheckForNull(strToCheck: dict.checkValueForKey(key: "status") as? String)
        self.candidateCompanyStatus = CheckForNull(strToCheck: dict.checkValueForKey(key: "company_status") as? String)
        self.candidateUnreadCount = CheckForNull(strToCheck: dict.checkValueForKey(key: "unreadCount") as? String)
    }

    class func storeCandidate(arraySeekers : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        arraySeekers.enumerateObjectsUsingBlock({ (obj , idx, stop) -> Void in
            let seeker : MyCandidate = MyCandidate(dict: obj as! NSDictionary)
            array.addObject(seeker)
        })
        return array
    }

    //MARK:- GET MY CANDIDATES
    class func getMyCandidates(data params : NSDictionary,completion :  (Bool ,AnyObject? ,NSError?) -> Void){
        BMServiceManager.fetchDataFromService("listusers", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,MyCandidate.storeCandidate(result["users"] as! NSArray) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No candidates found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }

}
