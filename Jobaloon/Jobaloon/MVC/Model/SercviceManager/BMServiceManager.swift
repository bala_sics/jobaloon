//
//  BMServiceManager.swift
//  ServiceManager
//
//  Created by Bala on 7/3/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class BMServiceManager: NSObject {
    var urlData : NSMutableData!
    var mainUrl : String!
    typealias completionBlock = (Bool? ,AnyObject? ,NSError?) -> Void
    var finishLoading: completionBlock!
    
    
    override init() {
        super.init()
        //setting main url
        mainUrl =  "http://54.175.128.115/jobaloon/json/"//"http://52.20.113.206/jobaloonNew/json/"
    }
    
    class func fetchDataFromService (serviceName: String, parameters : NSDictionary? , imageArray :  NSArray? , videoArray : NSArray?, withCompletion :(success : Bool, result: AnyObject, error: NSError?)-> Void){
        
        let serviceManager = BMServiceManager()
        serviceManager.httpRequestForService(serviceName, parameters: parameters, imageArray: imageArray, videoArray: videoArray) { (success, result, error) -> Void in
            withCompletion(success: success!, result: result!, error: error)
        }
    }
    
    func httpRequestForService(serviceName: String, parameters : NSDictionary?, imageArray : NSArray? , videoArray : NSArray?, completion :(success : Bool?, result: AnyObject?, error: NSError?)-> Void) {
        finishLoading = completion
        startConnection(baseUrl: String("\(mainUrl)\(serviceName)"), parameters: parameters, imageArray: imageArray, videoArray: videoArray)
    }
    
    func startConnection (baseUrl baseUrl : String , parameters : NSDictionary?, imageArray :  NSArray? , videoArray : NSArray?){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:  "text/html","application/json") as Set<NSObject>
        manager.POST(baseUrl, parameters: parameters, constructingBodyWithBlock: { (formData) -> Void in
            self.addImages(formData, imageArray: imageArray)
            self.addVideo(formData, videoArray: videoArray)
            }, success: { (operation, responseObject) -> Void in
            self.finishLoading(true,responseObject,nil)
        }) { (operation, error) -> Void in
            self.finishLoading(false,NSDictionary(),error)
        }
    }
  
    //MARK:- ADD IMAGES TO UPLOAD
    func addImages(formData : AFMultipartFormData , imageArray : NSArray?){
        if let arrayImages = imageArray{
            arrayImages.enumerateObjectsUsingBlock({ (dict, idx, stop) -> Void in
                let dictDetails : NSDictionary = dict as! NSDictionary
                let fileNameTemp : String = BMServiceManager.makeFileName() + ".jpg"
                print("Filename \(fileNameTemp)")
                (formData as AFMultipartFormData).appendPartWithFileData(dictDetails["value"] as! NSData, name: dictDetails["key"] as! String, fileName: fileNameTemp , mimeType: "image/jpeg")
            })
        }
    }
    //MARK:- MAKE FILE NAME
    class func makeFileName() -> String{
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyMMddHHmmssSSS"
        let dateString = dateFormatter.stringFromDate(NSDate())
        let randomValue  = arc4random() % 1000
        let fileName : String = "\(dateString)\(randomValue)"
        return fileName
    }

    //MARK:- ADD VIDEOS TO UPLOAD
    func addVideo(formData : AFMultipartFormData , videoArray : NSArray?){
        if let arrayVideos = videoArray{
            arrayVideos.enumerateObjectsUsingBlock({ (dict, idx, stop) -> Void in
                let dictDetails : NSDictionary = dict as! NSDictionary
                let fileNameTemp : String =  BMServiceManager.makeFileName() + ".mp4"
                print("Filename \(fileNameTemp)")
                (formData as AFMultipartFormData).appendPartWithFileData(dictDetails["value"] as! NSData, name: dictDetails["key"] as! String, fileName: fileNameTemp , mimeType: "video/mp4")
            })
        }
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////MIME TYPE FOR AUDIO "audio/x-caf"
}
