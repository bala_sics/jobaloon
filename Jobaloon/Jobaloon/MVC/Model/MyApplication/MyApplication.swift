//
//  MyApplication.swift
//  Jobaloon
//
//  Created by Bala on 9/25/15.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MyApplication: NSObject {
    
    var offerRole : String!
    var offerAdditionalInformation : String!
    var offerCompanyId : String!
    var offerEndDate : String!
    var offerId : String!
    var offerJobDescription : String!
    var offerLatitude : String!
    var offerLongitude : String!
    var offerLocation : String!
    var offerStartDate : String!
    var offerCompanyName : String!
    var offerWorkingDays : String!
    var offerImage : String!
    var offerStatus : String!
   // var companyDetails : CompanyDetails!
    
    init(dict : NSDictionary) {
        super.init()
        self.offerRole = CheckForNull(strToCheck: dict.checkValueForKey(key: "Role") as? String)
        self.offerAdditionalInformation = CheckForNull(strToCheck: dict.checkValueForKey(key: "additional_information") as? String)
        self.offerCompanyId = CheckForNull(strToCheck: dict.checkValueForKey(key: "compony_id") as? String)
        self.offerEndDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "end_date") as? String)
        self.offerId = CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)
        self.offerJobDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "jobDescription") as? String)
        self.offerLatitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "lattitude") as? String)
        self.offerLocation = CheckForNull(strToCheck: dict.checkValueForKey(key: "location") as? String)
        self.offerLongitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "longitude") as? String)
        self.offerStartDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "start_date") as? String)
        self.offerCompanyName = CheckForNull(strToCheck: dict.checkValueForKey(key: "user_name") as? String)
        self.offerWorkingDays = CheckForNull(strToCheck: dict.checkValueForKey(key: "working_days") as? String)
        self.offerImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
        self.offerStatus = CheckForNull(strToCheck: dict.checkValueForKey(key: "status") as? String)
       // self.companyDetails = CompanyDetails(dict: (dict["comapnyDetails"] as! NSArray)[0] as! NSDictionary)
    }

    
    //MARK:- GET MY APPLICATIONS
    class func getMyApplications(completion :  (Bool ,AnyObject? ,NSError?) -> Void){
        
        let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken , "userid" : Person.currentPerson().userId]
        BMServiceManager.fetchDataFromService("likedoffers", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,MyApplication.storeApplications(result["Details"] as! NSArray) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Applications found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }

    class func storeApplications(arrayApp : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        arrayApp.enumerateObjectsUsingBlock({ (obj , idx, stop) -> Void in
            let app : MyApplication = MyApplication(dict: obj as! NSDictionary)
            array.addObject(app)
        })
        return array
    }

    
    //MARK:- GET MY APPLICATIONS
    class func getMyCompanies(completion :  (Bool ,AnyObject? ,NSError?) -> Void){
           //52.20.113.206/jobaloonNew/json/Api.php?type=bothliked&userid=601
        let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken , "userid" : Person.currentPerson().userId]
        BMServiceManager.fetchDataFromService("bothliked", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,MyApplication.storeCompanies(result["Details"] as! NSArray) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Companies found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }
    
    class func storeCompanies(arrayApp : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        arrayApp.enumerateObjectsUsingBlock({ (obj , idx, stop) -> Void in
            let app : CompanyDetails = CompanyDetails(dict: obj as! NSDictionary)
            array.addObject(app)
        })
        return array
    }
    
    ////////////*********************** COMPANY DETAILS  **********************************/////////
    class CompanyDetails: NSObject {
        var username : String!
        var userId : String!
        var userEmail : String!
        var userPassword : String!
        var userLabels : String!
        var userCode : String!
        var userAddress : String!
        var userDateOfJoin : String!
        var userLastActivity : String!
        var userLastActivityDate : String!
        var userType : String!
        var userLatitude : String!
        var userLongitude : String!
        var userVideo : String!
        var userImage : String!
        var userNotes : String!
        var userStatus : String!
        var userJobId : String!
        var userJobDescription : String!
        var userExperience1 : String!
        var userExperience2 : String!
        var userExperience3 : String!
        var userDistance : String!
        var userLastMessage : String!
        var userMessageUnreadCount : String!
        
        init(dict : NSDictionary) {
            super.init()
            self.username =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_name") as? String)
            let userid = dict["user_id"] as! NSNumber
            self.userId =  "\(userid)"
            self.userEmail =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_email") as? String)
            self.userPassword = CheckForNull(strToCheck: dict.checkValueForKey(key: "password") as? String)
            self.userAddress =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_address") as? String)
            self.userLabels =  CheckForNull(strToCheck: dict.checkValueForKey(key: "job_type") as? String)
            self.userCode = CheckForNull(strToCheck: dict.checkValueForKey(key: "code") as? String)
            self.userType =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_type") as? String)
            self.userDateOfJoin =  CheckForNull(strToCheck: dict.checkValueForKey(key: "dateOfJoin") as? String)
            self.userLastActivity =  CheckForNull(strToCheck: dict.checkValueForKey(key: "Last_activity") as? String)
            self.userLastActivityDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "Last_activity_date") as? String)
            self.userLatitude =  CheckForNull(strToCheck: dict.checkValueForKey(key: "lattitude") as? String)
            self.userLongitude =  CheckForNull(strToCheck: dict.checkValueForKey(key: "longitude")as? String)
            self.userVideo = CheckForNull(strToCheck: dict.checkValueForKey(key: "video") as? String)
            self.userImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
            self.userNotes =  CheckForNull(strToCheck: dict.checkValueForKey(key: "Notes") as? String)
            self.userStatus = CheckForNull(strToCheck: dict.checkValueForKey(key: "user_status") as? String)
            self.userJobId = CheckForNull(strToCheck: dict.checkValueForKey(key: "JobId") as? String)
            self.userJobDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "Jobdescription") as? String)
            self.userExperience1 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience1") as? String)
            self.userExperience2 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience2") as? String )
            self.userExperience3 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience3") as? String)
            self.userDistance = CheckForNull(strToCheck: dict.checkValueForKey(key: "distance") as? String)
            self.userLastMessage = CheckForNull(strToCheck: dict.checkValueForKey(key: "Message") as? String)
            self.userMessageUnreadCount = CheckForNull(strToCheck: dict.checkValueForKey(key: "unreadCount") as? String)

        }

    }

}
