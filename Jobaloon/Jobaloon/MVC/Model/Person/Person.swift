//
//  Person.swift
//  Jobaloon
//
//  Created by Bala on 7/8/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

let videoPath : String = "http://54.175.128.115/jobaloon/uploads/videos/"
let imagePath : String = "http://54.175.128.115/jobaloon/uploads/images/"
var _person : Person?


class Person: NSObject {
    var username : String!
    var userId : String!
    var userEmail : String!
    var userPassword : String!
    var userLabels : String!
    var userCode : String!
    var userAddress : String!
    var userDateOfJoin : String!
    var userLastActivity : String!
    var userLastActivityDate : String!
    var userType : String!
    var userLatitude : String!
    var userLongitude : String!
    var userVideo : String!
    var userImage : String!
    var userNotes : String!
    var userStatus : String!
    var userJobId : String!
    var userJobDescription : String!
    var userExperience1 : String!
    var userExperience2 : String!
    var userExperience3 : String!
    var userDistance : String!
    var oauthToken : String!
    var offerCount : String!
    
    
    //MARK:- STORE USERDETAILS
    class func initWithDictionary(dict : NSDictionary) -> AnyObject
    {
        _person = Person()
        _person?.username =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_name") as? String)
        _person?.userId =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_id") as? String)
        _person?.userEmail =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_email") as? String)
        _person?.userPassword = CheckForNull(strToCheck: dict.checkValueForKey(key: "password") as? String)
        _person?.userAddress =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_address") as? String)
        _person?.userLabels =  CheckForNull(strToCheck: dict.checkValueForKey(key: "job_type") as? String)
        _person?.userCode = CheckForNull(strToCheck: dict.checkValueForKey(key: "code") as? String)
        _person?.userType =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_type") as? String)
        _person?.userDateOfJoin =  CheckForNull(strToCheck: dict.checkValueForKey(key: "dateOfJoin") as? String)
        _person?.userLastActivity =  CheckForNull(strToCheck: dict.checkValueForKey(key: "Last_activity") as? String)
        _person?.userLastActivityDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "Last_activity_date") as? String)
        _person?.userLatitude =  CheckForNull(strToCheck: dict.checkValueForKey(key: "lattitude") as? String)
        _person?.userLongitude =  CheckForNull(strToCheck: dict.checkValueForKey(key: "longitude")as? String)
        _person?.userVideo = CheckForNull(strToCheck: dict.checkValueForKey(key: "video") as? String)
        _person?.userImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
        _person?.userNotes =  CheckForNull(strToCheck: dict.checkValueForKey(key: "Notes") as? String)
        _person?.userStatus = CheckForNull(strToCheck: dict.checkValueForKey(key: "user_status") as? String)
        _person?.userJobId = CheckForNull(strToCheck: dict.checkValueForKey(key: "JobId") as? String)
        _person?.userJobDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "Jobdescription") as? String)
        _person?.userExperience1 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience1") as? String)
        _person?.userExperience2 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience2") as? String )
        _person?.userExperience3 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience3") as? String)
        _person?.userDistance = CheckForNull(strToCheck: dict.checkValueForKey(key: "distance") as? String)
        _person?.oauthToken = CheckForNull(strToCheck: dict.checkValueForKey(key: "oauthToken") as? String)
        let count =  dict.checkValueForKey(key: "offerCount") == nil ? 0 : dict["offerCount"] as! NSNumber
        _person?.offerCount = "\(count)"

        return _person!
    }
    
    class func userWithDetails (userDetails : NSDictionary) -> Person{
        
        if (_person == nil) {
            _person = Person.initWithDictionary(userDetails) as? Person
        }
        return _person!
    }
    
    //MARK:- CURRENT PERSON SINGLETON
    class func currentPerson() -> Person
    {
        if (_person == nil)
        {
            _person = Person()
        }
        return _person!
    }
    
    //MARK:- LOGOUT
    class func logout(){
        _person = nil
    }
    
    //MARK:- LOGIN
    class func login(email : String , password : String, completion :  (Bool ,AnyObject? ,NSError?) -> Void){
        let params : NSDictionary = [ "user_email" : email , "password" : password]
        BMServiceManager.fetchDataFromService("login", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,Person.userWithDetails((result["Details"] as! NSArray)[0] as! NSDictionary) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Invalid email or password", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }
    
    //MARK:- JOB LABELS
    class func getJobTypes(completion :  (Bool ,AnyObject? ,NSError?) -> Void){
        BMServiceManager.fetchDataFromService("labels", parameters: nil, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            success ? (result["Result"] as! Bool == true) ? completion(true,result["Label"] ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Job types found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }
    
    //MARK:- CREATE PROFILE
    class func createProfile(name name : String ,email : String,password : String,address : String , userType : String ,experience1 : String , exp2 : String , exp3 : String ,thumbnailImage : UIImage , videoData : NSData ,label : String,completion :  (Bool ,AnyObject? ,NSError?) -> Void){
        let params : NSDictionary = ["user_name" : name , "user_mail" : email , "user_password" : password , "user_address" : address , "user_type" : userType , "experience1" : experience1 , "experience2" : exp2 , "experience3" : exp3 , "job_type" : label , "distance" : USERDICT["distance"] as! String , "deviceToken" : ""]
        let imageData : NSData = UIImageJPEGRepresentation(thumbnailImage, 1.0)!
        BMServiceManager.fetchDataFromService("signup", parameters: params, imageArray: [["value": imageData , "key" : "image"]], videoArray: [["value" :videoData , "key" : "video"]]) { (success, result, error) -> Void in
            success ? (result["Result"] as! Bool == true) ? completion(true,Person.initWithDictionary((result["Details"] as! NSArray)[0] as! NSDictionary) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "User already exists", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }
    
    //MARK:- EDIT PROFILE
    class func editProfile(data params : NSDictionary ,image : NSArray,video : NSArray,completion :  (Bool ,AnyObject? ,NSError?) -> Void){

        BMServiceManager.fetchDataFromService("editprofile", parameters: params, imageArray:image , videoArray:video ) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,Person.initWithDictionary((result["Details"] as! NSArray)[0] as! NSDictionary) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "User already exists", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }

    
    //MARK:- CREATE COMPANY PROFILE
    class func createCompanyProfile(Data dict : NSDictionary , imageArray : NSArray! ,completion :  (Bool ,AnyObject? ,NSError?) -> Void){
        
        BMServiceManager.fetchDataFromService(dict["type"] as! String, parameters: dict, imageArray: imageArray, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,Person.initWithDictionary((result["Details"] as! NSArray)[0] as! NSDictionary) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "User already exists", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }
    
    //MARK:- LOGOUT
    class func logout(completion :  (Bool ,NSError?) -> Void){
        let dataDict = ["oauthToken" : Person.currentPerson().oauthToken , "userid" : Person.currentPerson().userId]
        BMServiceManager.fetchDataFromService("logout", parameters: dataDict, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,error)
        }
    }
    
}
