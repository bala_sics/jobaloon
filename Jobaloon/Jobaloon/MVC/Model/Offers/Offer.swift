//
//  Offer.swift
//  Jobaloon
//
//  Created by Bala on 7/14/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit

class Offer: NSObject {
    
    var offerRole : String!
    var offerAdditionalInformation : String!
    var offerCompanyId : String!
    var offerEndDate : String!
    var offerId : String!
    var offerJobDescription : String!
    var offerLatitude : String!
    var offerLongitude : String!
    var offerLocation : String!
    var offerStartDate : String!
    var offerUserName : String!
    var offerWorkingDays : String!
    var offerImage : String!
    var offerName : String!
    var offerName1 : String!
    var offerName2 : String!
    var offerName3 : String!
    var offerDescription1 : String!
    var offerDescription2 : String!
    var offerDescription3 : String!
    
    init(dict : NSDictionary) {
        super.init()
        self.offerRole = CheckForNull(strToCheck: dict.checkValueForKey(key: "Role") as? String)
        self.offerAdditionalInformation = CheckForNull(strToCheck: dict.checkValueForKey(key: "additional_information") as? String)
        self.offerCompanyId = CheckForNull(strToCheck: dict.checkValueForKey(key: "compony_id") as? String)
        self.offerEndDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "end_date") as? String)
        self.offerId = CheckForNull(strToCheck: dict.checkValueForKey(key: "offerid") as? String)
        self.offerJobDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "jobDescription") as? String)
        self.offerLatitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "lattitude") as? String)
        self.offerLocation = CheckForNull(strToCheck: dict.checkValueForKey(key: "location") as? String)
        self.offerLongitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "longitude") as? String)
        self.offerStartDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "start_date") as? String)
        self.offerUserName = CheckForNull(strToCheck: dict.checkValueForKey(key: "user_name") as? String)
        self.offerWorkingDays = CheckForNull(strToCheck: dict.checkValueForKey(key: "working_days") as? String)
        self.offerImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
        self.offerName = CheckForNull(strToCheck: dict.checkValueForKey(key: "offer_name") as? String)
        self.offerName1 = CheckForNull(strToCheck: dict.checkValueForKey(key: "offer1") as? String)
        self.offerName2 = CheckForNull(strToCheck: dict.checkValueForKey(key: "offer2") as? String)
        self.offerName3 = CheckForNull(strToCheck: dict.checkValueForKey(key: "offer3") as? String)
        self.offerDescription1 = CheckForNull(strToCheck: dict.checkValueForKey(key: "jobDescription1") as? String)
        self.offerDescription2 = CheckForNull(strToCheck: dict.checkValueForKey(key: "jobDescription2") as? String)
        self.offerDescription3 = CheckForNull(strToCheck: dict.checkValueForKey(key: "jobDescription3") as? String)


    }
    
    //MARK:- JOB OFFERS
    class func getJobOffers(jobType jobType : String , address : String ,completion :  (Bool ,AnyObject? ,NSError?) -> Void){
        //52.0.125.31/JobBox/json/Api.php?type=searchByJobseeker&label=&fromaddress=
        let distance : String = Person.currentPerson().userId == nil ? USERDICT["distance"] as! String : Person.currentPerson().userDistance
        let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken == nil ? "" : Person.currentPerson().oauthToken  , "label" : jobType , "user_address" : address , "userid" : Person.currentPerson().userId == nil ? "" : Person.currentPerson().userId , "distance" : distance]
        BMServiceManager.fetchDataFromService("searchjobseeker", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,Offer.storeOffers(result["Details"] as! NSArray) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No offers found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }
    
    class func storeOffers(arrayOffers : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        arrayOffers.enumerateObjectsUsingBlock({ (obj , idx, stop) -> Void in
            let offerObj : Offer = Offer(dict: obj as! NSDictionary)
            array.addObject(offerObj)
        })
        return array
    }
    
    //MARK:- LIKE OR DISLIKE OFFERS
    class func likeOrDislikeJobOffers(jobId jobId : String , likestatus : String ,completion :  (Bool ,NSError?) -> Void){
        let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken , "userid" : Person.currentPerson().userId , "offerid" : jobId , "status" : likestatus]
        BMServiceManager.fetchDataFromService("likeoffer", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            success ? (result["result"] as! Bool == true) ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,error)
        }
    }
    
    //MARK:- CREATE JOB OFFERS
    class func createJobOffers(data param : NSDictionary ,completion :  (Bool ,NSError?) -> Void){
        BMServiceManager.fetchDataFromService("jobPost", parameters: param, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            success ? (result["Result"] as! Bool == true) ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,error)
        }
    }

    //MARK:- MY OFFERS
    class func myOffers(Data params : NSDictionary ,completion :  (Bool ,AnyObject? ,NSError?) -> Void){
        BMServiceManager.fetchDataFromService("viewjobPosts", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,Offer.storeOffers(result["Details"] as! NSArray) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No offers found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }
  
    //MARK:- JOBSEEKERS
    class JobSeekers: NSObject {
        var username : String!
        var userId : String!
        var userEmail : String!
        var userPassword : String!
        var userLabels : String!
        var userCode : String!
        var userAddress : String!
        var userDateOfJoin : String!
        var userLastActivity : String!
        var userLastActivityDate : String!
        var userType : String!
        var userLatitude : String!
        var userLongitude : String!
        var userVideo : String!
        var userImage : String!
        var userNotes : String!
        var userStatus : String!
        var userJobId : String!
        var userJobDescription : String!
        var userExperience1 : String!
        var userExperience2 : String!
        var userExperience3 : String!
        var userDistance : String!

        init(dict : NSDictionary) {
            super.init()
            self.username =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_name") as? String)
            self.userId =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_id") as? String)
            self.userEmail =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_email") as? String)
            self.userPassword = CheckForNull(strToCheck: dict.checkValueForKey(key: "password") as? String)
            self.userAddress =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_address") as? String)
            self.userLabels =  CheckForNull(strToCheck: dict.checkValueForKey(key: "job_type") as? String)
            self.userCode = CheckForNull(strToCheck: dict.checkValueForKey(key: "code") as? String)
            self.userType =  CheckForNull(strToCheck: dict.checkValueForKey(key: "user_type") as? String)
            self.userDateOfJoin =  CheckForNull(strToCheck: dict.checkValueForKey(key: "dateOfJoin") as? String)
            self.userLastActivity =  CheckForNull(strToCheck: dict.checkValueForKey(key: "Last_activity") as? String)
            self.userLastActivityDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "Last_activity_date") as? String)
            self.userLatitude =  CheckForNull(strToCheck: dict.checkValueForKey(key: "lattitude") as? String)
            self.userLongitude =  CheckForNull(strToCheck: dict.checkValueForKey(key: "longitude")as? String)
            self.userVideo = CheckForNull(strToCheck: dict.checkValueForKey(key: "video") as? String)
            self.userImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
            self.userNotes =  CheckForNull(strToCheck: dict.checkValueForKey(key: "Notes") as? String)
            self.userStatus = CheckForNull(strToCheck: dict.checkValueForKey(key: "user_status") as? String)
            self.userJobId = CheckForNull(strToCheck: dict.checkValueForKey(key: "JobId") as? String)
            self.userJobDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "Jobdescription") as? String)
           self.userExperience1 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience1") as? String)
            self.userExperience2 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience2") as? String )
            self.userExperience3 = CheckForNull(strToCheck: dict.checkValueForKey(key: "experience3") as? String)
            self.userDistance = CheckForNull(strToCheck: dict.checkValueForKey(key: "distance") as? String)
        }

        //MARK:- GET JOB SEEKERS
        class func getJobSeekers(data params : NSDictionary , completion :  (Bool ,AnyObject? ,NSError?) -> Void){
            BMServiceManager.fetchDataFromService("jobseekers", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
                print(result)
                success ? (result["Result"] as! Bool == true) ? completion(true,JobSeekers.storeJobSeekers(result["Details"] as! NSArray) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Job seekers found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
            }
        }
        
        class func storeJobSeekers(arraySeekers : NSArray) -> NSArray
        {
            let array = NSMutableArray()
            arraySeekers.enumerateObjectsUsingBlock({ (obj , idx, stop) -> Void in
                let seeker : JobSeekers = JobSeekers(dict: obj as! NSDictionary)
                array.addObject(seeker)
            })
            return array
        }
        
        //MARK:- LIKE OR DISLIKE OFFERS
        class func likeOrDislikeJobSeekers(jobSeekerId jobSeekerId : String , likestatus : String ,completion :  (Bool ,NSError?) -> Void){
            let params : NSDictionary = ["oauthToken" : Person.currentPerson().oauthToken , "userid" : Person.currentPerson().userId , "to_userid" : jobSeekerId , "status" : likestatus]
            BMServiceManager.fetchDataFromService("like", parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
                print(result)
                success ? (result["Result"] as! Bool == true) ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,error)
            }
        }
    }
}
