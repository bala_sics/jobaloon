//
//  Message.swift
//  Jobaloon
//
//  Created by Bala on 9/28/15.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class Message: NSObject {

    var messageId : String!
    var messageText : String!
    var messageImage : UIImage!
    var messageTime : String!
    var userId : String!
    var userName : String!
    var profileImageStr : String!
    var messageImageStr : String!
    
    
    class func initWithDictionary (dict : NSDictionary) -> Message{
        let messg : Message = Message()
        let chatid = dict["id"] as! NSNumber
        messg.messageId = "\(chatid)"
        messg.messageText =  CheckForNull(strToCheck: dict.checkValueForKey(key: "Message") as? String)
        let userid = dict["fromId"] as! NSNumber
        messg.userId =  "\(userid)"
        messg.messageImageStr =  CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
        messg.messageTime =  CheckForNull(strToCheck: dict.checkValueForKey(key: "date") as? String)
//        messg.userName =  CheckForNull(strToCheck: dict.checkValueForKey(key: "username") as? String)
//        messg.profileImageStr =  CheckForNull(strToCheck: dict.checkValueForKey(key: "profilePic") as? String)
        return messg
        
    }
    
    //MARK:- INSERT MESSAGE
    class func insertMessage(params : NSDictionary , image : UIImage?, completion :  (Bool ,NSError?) -> Void){
       var imageArray : NSArray!
        if let imageAttached = image
        {
            let imageData : NSData = UIImageJPEGRepresentation(imageAttached, 0.6)!
            imageArray =  [["value": imageData , "key" : "image"]]

        }else{
            imageArray = []
        }
        BMServiceManager.fetchDataFromService("chatting", parameters: params, imageArray: imageArray, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            success ? (result["Result"] as! Bool == true) ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,error)
        }
    }
    
    //MARK:- INSERT MESSAGE
    class func listMessage(params : NSDictionary , completion :  (Bool ,AnyObject?, NSError?) -> Void){
        
        BMServiceManager.fetchDataFromService(params["type"] as! String, parameters: params, imageArray: nil, videoArray: nil) { (success, result, error) -> Void in
            print(result)
            let status : Bool = success ? result["Result"] as! Bool  : false
            success ? (status == true) ? completion(true,Message.storeMessages(result["Chat"] as! NSArray),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Messages", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject])) : completion(false,nil,error)
        }
    }

    class func storeMessages(arrayMessage : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        (arrayMessage.reverseObjectEnumerator().allObjects as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let messg : Message = Message.initWithDictionary(obj as! NSDictionary)
            array.addObject(messg)
        }
        return array
    }

}
