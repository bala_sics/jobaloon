//
//  BMLabel.swift
//  Jobaloon
//
//  Created by Bala on 7/14/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import Foundation
import UIKit

extension UILabel{
    
    //MARK:- CALCULATE HEIGHT FOR LABEL
    func heightForLabel ( text:String, font:UIFont, width:CGFloat) -> CGSize{
        self.frame = CGRectMake(0, 0, width, CGFloat.max)
        self.numberOfLines = 0
        self.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.font = font
        self.text = text
        self.sizeToFit()
        return self.frame.size
    }
    
}