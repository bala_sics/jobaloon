//
//  BMDictionary.swift
//  Jobaloon
//
//  Created by Bala on 9/2/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import Foundation

extension NSDictionary {
    
    func checkValueForKey(key key: Key) -> Value? {
        // if key not found, replace the nil with
        // the first element of the values collection
        return self[key] ?? nil
        // note, this is still an optional (because the
        // dictionary could be empty)
    }
    
//    TO CHECK
//    let d = ["one":"red", "two":"blue"]
//    let string : String! = CheckForNull(strToCheck: d.checkValueForKey(key: "one"))
    
}