//
//  UItextfieldPadding.swift
//  Jobaloon
//
//  Created by Bala on 7/8/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    
    func setLeftPadding (paddingValue : CGFloat){
        let paddingView : UIView = UIView(frame: CGRectMake(0, 0, paddingValue, self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.Always
    }
   
    func setRightPadding (paddingValue : CGFloat){
        let paddingView : UIView = UIView(frame: CGRectMake(0, 0, paddingValue, self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = UITextFieldViewMode.Always
    }

    //MARK:- EMAIL VALIDATION
    func IsValidEmail() -> Bool
    {
        let stricterFilterString:NSString = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailRegex:NSString = stricterFilterString
        let emailTest:NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluateWithObject(self.text)
    }
}