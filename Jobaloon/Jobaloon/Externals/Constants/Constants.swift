//
//  Constants.swift
//  Jobaloon
//
//  Created by Bala on 7/8/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import Foundation
import UIKit

//SCREEN SIZE
let screenSize = UIScreen.mainScreen().bounds

//IOS VERSION
let Device = UIDevice.currentDevice()
private let iosVersion = NSString(string: Device.systemVersion).doubleValue
let IS_iOS8 : Bool = iosVersion >= 8
let IS_iOS7 : Bool = iosVersion >= 7 && iosVersion < 8

//CHECK USER DEVICE
let IS_IPHONE4 : Bool = screenSize.height == 480
let IS_IPHONE5 : Bool = screenSize.height == 568
let IS_IPHONE6 : Bool = screenSize.height == 667
let IS_IPHONE6PLUS : Bool = screenSize.height == 736

//FONT NAME
let HeroLight : String = "Hero Light"
let Helvetica : String = "Helvetica Neue"

//GOOGLE PLACES API KEY
let GOOGLE_APIKEY : String = "AIzaSyDfzPH8vWSZNKhtnAjRJCz04Jxv2Mac4gw"

//USER DETAILS BEFORE LOGIN
var USERDICT : NSMutableDictionary = NSMutableDictionary()
var USER_LOCATION : CLLocation = CLLocation(latitude: 0.0, longitude: 0.0)

func CheckForNull(strToCheck strToCheck : String?) -> String{
    if let strText = strToCheck {
        return strText
    }
    return "nil"
}
