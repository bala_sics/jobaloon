//
//  NSDate+TimeZone.m
//  Companion
//
//  Created by Sics on 11/6/13.
//  Copyright (c) 2013 Sics. All rights reserved.
//


#import "NSDate+TimeZone.h"
#define D_MINUTE	60

@implementation NSDate (TimeZone)

+ (NSString *)changeServerTimeZoneToLocalForDate:(NSString *)stringServerDate
{
    //Date formatter to convert GMT into local time
    NSDateFormatter *serverDateFormatter2Local = [[NSDateFormatter alloc] init];
    [serverDateFormatter2Local setDateFormat:@"MMM dd,yyyy hh:mm a"];
    [serverDateFormatter2Local setTimeZone:[NSTimeZone systemTimeZone]];
    //NSLog(@"SystemTimeZone : %@",[NSTimeZone systemTimeZone]);
    
    //Date formatter to convert GMT string into GMT date
    NSDateFormatter *serverDateFormatter = [[NSDateFormatter alloc] init];
    [serverDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [serverDateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *serverDate = [serverDateFormatter dateFromString:stringServerDate];
    NSString *serverDateConverted = [serverDateFormatter2Local stringFromDate:serverDate];
    return serverDateConverted;
}

+ (NSString *)changeLocalTimeZoneToServerForDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"America/New_York"]];
    NSString *serverTime = [dateFormatter stringFromDate:[NSDate date]];
    return serverTime;
}

+ (NSString *)changeServerTimeZoneToLocalMediumFormatForDate:(NSString *)stringServerDate
{
    //Date formatter to convert GMT into local time
    NSDateFormatter *serverDateFormatter2Local = [[NSDateFormatter alloc] init];
    [serverDateFormatter2Local setDateFormat:@"MMM dd"];
    [serverDateFormatter2Local setTimeZone:[NSTimeZone systemTimeZone]];
    //NSLog(@"SystemTimeZone : %@",[NSTimeZone systemTimeZone]);
    
    //Date formatter to convert GMT string into GMT date
    NSDateFormatter *serverDateFormatter = [[NSDateFormatter alloc] init];
    [serverDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [serverDateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"CET"]];
    
    NSDate *serverDate = [serverDateFormatter dateFromString:stringServerDate];
    NSString *serverDateConverted = [serverDateFormatter2Local stringFromDate:serverDate];
    
    return serverDateConverted;
}

@end